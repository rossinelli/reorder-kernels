# reorder-kernels

## Goal
Tiny data reordering and transpositions are the building blocks for effetively exploiting the SIMD nature of current x86_64 CPUs.
This repo provides xx- xy- and xz- transpositions of memory layouts with a footprint of 10 MB or less.
This repo is built from scratch using a bottom up HPC approach. 
The goal of this repository is to generate object files that can be in turn linked against other, larger binaries. 

## Example
Let's suppose you are developing code in ```~/mycode/``` and the repo is installed in ```~/reorder-kernels```. 
From ```~/mycode/``` you could generate an AVX-enabled xy-transposition of 32x32 with:
```
make -C ~/reorder-kernels/transpose-xy size=32 name=mycode_XYT32
```
The call will deploy the object file ```~/mycode/transpose-xy-32-avx.o```. 
The object file will have just one function: ```mycode_XYT32```.

## Current limitations:
- Domain sizes resolved at compile time.
- Support only for C and AVX/AVX2.