#MPI
export CPATH=$CPATH:/usr/include/mpich-3.2-x86_64/
export LIBRARY_PATH=$LIBRARY_PATH:/usr/lib64/mpich-3.2/lib/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib64/mpich-3.2/lib/
export PATH=$PATH:/lib64/mpich-3.2/bin/
export LDFLAGS=" -lnuma -lmpi "

#reorder-kernels
CPATH=$CPATH:$HOME/CLIP.core/share/
CPATH=$CPATH:$HOME/reorder-kernels/aos/lib
CPATH=$CPATH:$HOME/reorder-kernels/transpose-xy/library
CPATH=$CPATH:$HOME/reorder-kernels/transpose-mpi
CPATH=$CPATH:$HOME/CLIP.core/share/nothreads/library
LIBRARY_PATH=$LIBRARY_PATH:$HOME/reorder-kernels/aos/lib/
LIBRARY_PATH=$LIBRARY_PATH:$HOME/reorder-kernels/transpose-xy/library
LIBRARY_PATH=$LIBRARY_PATH:$HOME/reorder-kernels/transpose-mpi
LIBRARY_PATH=$LIBRARY_PATH:$HOME/CLIP.core/share/nothreads/library

export CFLAGS=" -Ofast -march=core-avx2 -DNDEBUG "
export CXXFLAGS=" -Ofast -march=core-avx2 -DNDEBUG "
export CC="$HOME/gcc-latest/bin/gcc"
