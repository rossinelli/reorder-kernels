#include <stddef.h>

typedef struct
{
    MPI_Comm comm;
    ptrdiff_t xnpr, ynpr, yxnpr;
    ptrdiff_t x0, y0;
    ptrdiff_t xn, yn;
    ptrdiff_t xnpad, ynpad;
    ptrdiff_t eln;
    int shm;
} transpose_t;

transpose_t transpose_init (
    MPI_Comm comm,
    const ptrdiff_t yn,
    const ptrdiff_t xn);

transpose_t transpose_init_ext (
    MPI_Comm comm,
    const ptrdiff_t yn,
    const ptrdiff_t xn,
    const int hint_shared_memory );

void transpose_mpi (
    const transpose_t w,
    const ptrdiff_t elsz,
    void * const __restrict__ src_destructive,
    void * const __restrict__ dst );
