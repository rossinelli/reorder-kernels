#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include <string.h>

#include <mpi.h>
#include <mpi-util.h>

#include "transpose-mpi.h"

#ifdef _NOTHREADS_
#include <nt.h>
#endif

#ifndef MAX
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#endif

#ifndef MIN
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#endif

#define CHECK(stmt, ...)						\
	do											\
	{											\
		if (!(stmt))							\
		{										\
			fprintf(stderr,						\
					__VA_ARGS__);				\
												\
			exit(EXIT_FAILURE);					\
		}										\
	}											\
	while(0)

#define READENV(x, op)							\
	do											\
	{											\
		if (getenv(#x))							\
			x = op(getenv(#x));					\
	}											\
	while(0)

#define PRINTENV(f, x, p)						\
	fprintf(f, #x ": " p "\n", x)


int main (
	int argc,
	char ** argv)
{
	int VERBOSE = 0;
	READENV(VERBOSE, atoi);

	int NTIMES = 1;
	READENV(NTIMES, atoi);

	int FINEGRAIN = 0;
	READENV(FINEGRAIN, atoi);

	MPI_CHECK(MPI_Init(&argc, &argv));

#ifdef _NOTHREADS_
	nt_init();
	//nt_batch();
	MPI_Comm comm = nt_node.comm;

#else
	MPI_Comm comm = MPI_COMM_WORLD;
#endif

	int r, rn;
	MPI_CHECK(MPI_Comm_rank(comm, &r));
	MPI_CHECK(MPI_Comm_size(comm, &rn));

	if (argc != 3)
	{
		if (r == 0)
			fprintf(stderr,
					"usage: %s <xsize> <ysize>\n",
					argv[0]);

		return r ? EXIT_SUCCESS : EXIT_FAILURE;
	}

	if (!r)
	{
		printf("sizeof(real): %d\n"
			   "proceeding with %d MPI tasks\n",
			   sizeof(int32_t), rn);

		fprintf(stdout, "ENV VARS:\n");

		PRINTENV(stdout, NTIMES, "%d");
		PRINTENV(stdout, VERBOSE, "%d");
		PRINTENV(stdout, FINEGRAIN, "%d");
	}

	const ptrdiff_t xn = atoi(argv[1]);
	const ptrdiff_t yn = atoi(argv[2]);


#ifdef _NOTHREADS_
	int SHM = 1;
	READENV(SHM, atoi);
#else
	const int SHM = 0;
#endif

	transpose_t tinfo = transpose_init_ext(comm, yn, xn, SHM);

	CHECK(xn == tinfo.xn && yn == tinfo.yn,
		  "error: size mismatch\n");

	const ptrdiff_t __attribute__((unused)) xnpr = tinfo.xnpr;
	const ptrdiff_t __attribute__((unused)) ynpr = tinfo.ynpr;
	const ptrdiff_t __attribute__((unused)) yxnpr = tinfo.yxnpr;
	const ptrdiff_t __attribute__((unused)) x0 = tinfo.x0;
	const ptrdiff_t __attribute__((unused)) y0 = tinfo.y0;
	const ptrdiff_t __attribute__((unused)) xnpad = tinfo.xnpad;
	const ptrdiff_t __attribute__((unused)) ynpad = tinfo.ynpad;

	if (VERBOSE)
		fprintf(stderr,
				"hi from rank %d of %d\n", r, rn);

	if (!r)
		fprintf(stderr, "padded image: %d x %d, tile: %d x %d \n",
				xnpad, ynpad, xnpr, ynpr);

#ifdef _NOTHREADS_
	int32_t *data_all = NULL, *data = NULL;

	if (2 == SHM)
	{
		data_all = nt_shmem(nt_node, sizeof(int32_t) * rn * yxnpr * rn);
		data = data_all + ynpad * xnpr * r;
	}
	else
		data = malloc(sizeof(int32_t) * rn * yxnpr);

	int32_t * tmp_all = nt_shmem(nt_node, sizeof(int32_t) * rn * yxnpr * rn);
	int32_t * tmp = tmp_all + xnpad * ynpr * r;
#else
	int32_t * data = malloc(sizeof(int32_t) * rn * yxnpr);
	int32_t * tmp = malloc(sizeof(int32_t) * rn * yxnpr);
#endif

	for (ptrdiff_t y = 0; y < MIN(ynpr, yn - y0); ++y)
		for (ptrdiff_t x = 0; x < xn; ++x)
			tmp[x + xnpad * y] = x + xn * (y + y0);

	transpose_mpi(tinfo, 4, tmp, data);

	for (ptrdiff_t x = 0; x < MIN(xnpr, xn - x0); ++x)
		for(ptrdiff_t y = 0; y < yn; ++y)
		{
			const uint32_t entry = y + ynpad * x;
			const uint32_t expected = x0 + x + xn * y;

			CHECK(data[entry] == expected,
				  "oops rank %d not as expected: data[%d] = %d and not %d\n",
				  r, entry, data[entry], expected);
		}

	MPI_CHECK(MPI_Barrier(comm));

	const double t0 = MPI_Wtime();

	for(int t = 0; t < NTIMES; ++t)
		transpose_mpi(tinfo, 4, tmp, data);

	MPI_CHECK(MPI_Barrier(comm));

	const double tavg = (MPI_Wtime() - t0) / NTIMES;

	if (!r)
	{
		fprintf(stderr, "average transpose time: %.3fms\n", 1e3 * tavg);
		fprintf(stderr, "BW: %.3f GB/s\n", xn * yn * sizeof(int32_t) * 2e-9 / tavg);
	}

#ifdef _NOTHREADS_
	nt_free(tmp_all);

	if (data_all)
		nt_free(data_all);
	else
		free(data);
#else
	free(tmp);
	free(data);
#endif
	MPI_CHECK(MPI_Finalize());

	return EXIT_SUCCESS;
}
