#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#define _TILESZ_ 4

#include <stddef.h>
#include <stdint.h>

static void swap (
    const ptrdiff_t n,
    uint8_t * const __restrict__ inout0,
    uint8_t * const __restrict__ inout1 )
{
#pragma GCC unroll (2)
    for (ptrdiff_t i = 0; i < n; ++i)
    {
        const uint8_t tmp = inout0[i];

	inout0[i] = inout1[i];
        inout1[i] = tmp;
    }
}

static void trsq (
    const int n,
    const int elsz,
    uint8_t * const __restrict__ data)
{
    for (ptrdiff_t xt = 0; xt < n; xt += _TILESZ_)
    {
	const ptrdiff_t xln = MIN(_TILESZ_, n - xt);

	for (ptrdiff_t yt = 0; yt <= xt; yt += _TILESZ_)
	    for (ptrdiff_t xl = 0; xl < xln; ++xl)
	    {
		const ptrdiff_t x = xt + xl;

		/* y ranges from yt to x (excluded) */
		const ptrdiff_t yln = MIN (_TILESZ_, x - yt);

		for (ptrdiff_t yl = 0; yl < yln; ++yl)
		{
		    const ptrdiff_t y = yt + yl;

		    swap(elsz,
			 data + elsz * (x + n * y),
			 data + elsz * (y + n * x));
		}
	    }
    }
}

#include <string.h>

__attribute__((visibility("default")))
void transpose_xyv (
    void * const __restrict__ src,
    const int yn,
    const int xn,
    const int elsz,
    void * const __restrict__ dst )
{
    if (yn == xn)
    {
	uint8_t * data = (uint8_t *)src;

	if (dst && dst != src)
	{
	    memcpy(dst, src, elsz * (ptrdiff_t)xn * (ptrdiff_t)yn);
	    data = (uint8_t *)dst;
	}

	trsq(xn, elsz, data);

	return;
    }

    for (ptrdiff_t xt = 0; xt < xn; xt += _TILESZ_)
    {
	const ptrdiff_t xln = MIN(_TILESZ_, xn - xt);

	for (ptrdiff_t yt = 0; yt < yn; yt += _TILESZ_)
	{
	    const ptrdiff_t yln = MIN(_TILESZ_, yn - yt);

	    for (ptrdiff_t xl = 0; xl < xln; ++xl)
	    {
		const ptrdiff_t x = xt + xl;

		for (ptrdiff_t yl = 0; yl < yln; ++yl)
		{
		    const ptrdiff_t y = yt + yl;

		    memcpy(elsz * (y + yn * x) + (uint8_t *)dst,
			   elsz * (x + xn * y) + (uint8_t *)src,
			   elsz);
		}
	    }
	}
    }
}
