#!/usr/bin/env bash

function run()
{
    ename=$1
    xn=$2
    yn=$3
    echo "================== $ename $xn x $yn =================="
    for t in `seq 1 10` ; do sleep .5; $ename $xn $yn | grep TTS ; done | sort -g -k2,2
}

function assess()
{
    ename=$1
    run $ename 4240 3500
    run $ename 3500 4240
}

assess ./test-xyt.fp64
assess ./test-xyt.fp32
assess ./test-xyt.i8
