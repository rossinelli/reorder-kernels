#pragma once

void transpose_xyd (
    void * const __restrict__ in,
    const int yn_in,
    const int xn_in,
    void * const __restrict__ out );

void transpose_xyf (
    void * const __restrict__ in,
    const int yn_in,
    const int xn_in,
    void * const __restrict__ out );

void transpose_xyf_panel (
    const void * const __restrict__ in,
    const int yn_in,
    const int xn_in,
    const int panelsize,
    void * const __restrict__ out );

#include <stdint.h>

void transpose_xys (
    void * const __restrict__ in,
    const int yn_in,
    const int xn_in,
    void * const __restrict__ out );

void transpose_xyc (
    void * const __restrict__ in,
    const int yn_in,
    const int xn_in,
    void * const __restrict__ out );

void transpose_xyv (
    void * const __restrict__ in,
    int yn_in,
    const int xn_in,
    const int szel,
    void * const __restrict__ out );
