#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include <sys/time.h>

#ifdef _USEMPI_
#include <mpi.h>
#define _BARRIER_() MPI_Barrier(comm)
#else
#define _BARRIER_()
#endif

#include <sched.h>

#include <common-util.h>
#include <posix-util.h>

#include "transpose-xy.h"

#define CAT(x,y) x ## y

#ifdef _I8_
typedef int8_t real;
#define LIBCALL(x) CAT(x, c)
#elif defined(_I16_)
typedef int16_t real;
#define LIBCALL(x) CAT(x, s)
#elif defined(_FP32_)
typedef float real;
#define LIBCALL(x) CAT(x, f)
#else
typedef double real;
#define LIBCALL(x) CAT(x, d)
#endif

#if 1
int64_t get_cycles(void)
{
    unsigned int lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}
#else
// rdpmc_actual_cycles uses a "fixed-function" performance counter to return the count of actual CPU core cycles
//       executed by the current core.  Core cycles are not accumulated while the processor is in the "HALT" state,
//       which is used when the operating system has no task(s) to run on a processor core.
int64_t get_cycles()
{
    unsigned a, d, c;

    c = (1<<30)+1;
    __asm__ volatile("rdpmc" : "=a" (a), "=d" (d) : "c" (c));

    return ((size_t)a) | (((size_t)d) << 32ull);
}
#endif

int main (
    const int argc,
    const char * const argv[] )
{
#ifdef _USEMPI_
    MPI_Init((int *)&argc, (char ***)&argv);
    MPI_Comm comm = MPI_COMM_WORLD;

    int r, rn;
    MPI_Comm_rank(comm, &r);
    MPI_Comm_size(comm, &rn);
#else
    const int r = 0;
    const int rn = 1;
#endif

#ifndef _WIN32
    {
	/* pin the process to make sure rdpmc values are consistent */
	cpu_set_t cpu_set;
	CPU_ZERO(&cpu_set);
	CPU_SET(r, &cpu_set);
	if (sched_setaffinity(0, sizeof(cpu_set), &cpu_set) < 0)
	    fprintf(stderr, "cannot set cpu affinity\n");
    }
#endif

    if (argc != 3)
    {
	if (!r)
	    fprintf(stderr,
		    "usage: %s <xsize> <ysize>\n",
		    argv[0]);

#ifdef _USEMPI_
    MPI_Finalize();
#endif

	return EXIT_FAILURE;
    }

    const int xn = atoi(argv[1]);
    const int yn = atoi(argv[2]);
    const int yxn = yn * xn;

    real * in, * out;
#ifdef _WIN32
    in = malloc(sizeof(real) * yxn);
    out = malloc(sizeof(real) * yxn);
#else
    POSIX_CHECK(0 == posix_memalign((void **)&in, 4096, sizeof(real) * yxn));
    POSIX_CHECK(0 == posix_memalign((void **)&out, 4096, sizeof(real) * yxn));
#endif
    for(int y = 0; y < yn; ++y)
	for(int x = 0; x < xn; ++x)
	{
	    int entry = x + xn * y;
#ifdef _I8_
	    in[entry] = entry & 0xff;
#elif defined(_I16_)
	    in[entry] = entry & 0xffff;
#else
	    in[entry] = entry;
#endif

	}

    if (yn == xn)
    {
	memcpy(out, in, sizeof(real) * yxn);
	LIBCALL(transpose_xy)(out, yn, xn, NULL);
    }
    else
	LIBCALL(transpose_xy)(in, yn, xn, out);

    for(int x = 0; x < xn; ++x)
	for(int y = 0; y < yn; ++y)
	{
	    int entry = y + yn * x;
	    int expected = x + xn * y;

#ifdef _I8_
	    expected &= 0xff;

	    CHECK(expected == (uint8_t)out[entry],
		  "error: %d != out[%d] (%d) at x: %d, y: %d\n",
		  expected, entry, (uint8_t)out[entry], x, y);
#elif defined(_I16_)
	    expected &= 0xffff;

	    CHECK(expected == (uint16_t)out[entry],
		  "error: %d != out[%d] (%d) at x: %d, y: %d\n",
		  expected, entry, (uint16_t)out[entry], x, y);

#else
	    CHECK(expected == out[entry],
		  "error: %d != out[%d] (%d) at x: %d, y: %d\n",
		  expected, entry, (int)out[entry], x, y);

#endif
	}

    if (!r)
	printf("accuracy test passed.\n");

    int rep = 1;

    if (getenv("REP"))
	rep = atoi(getenv("REP"));

    int tn = (int)((15000 * 1e6) / (yxn * sizeof(real) * 2));

    if (getenv("TN"))
	tn = atoi(getenv("TN"));

    struct timeval t0, t1;

    long double tts = 0, cc = 0;

    for (int rr = 0; rr < rep; ++rr)
    {
    measure:
	_BARRIER_();
	const int64_t c0 = get_cycles();
	POSIX_CHECK(0 == gettimeofday(&t0, NULL));

	if (yn == xn)
	    for(int t = 0; t < tn; ++t)
		LIBCALL(transpose_xy)(out, yn, xn, NULL);
	else
	    for(int t = 0; t < tn; ++t)
		LIBCALL(transpose_xy)(in, yn, xn, out);

	POSIX_CHECK(0 == gettimeofday(&t1, NULL));
	const int64_t c1 = get_cycles();
	_BARRIER_();


	int error = (c1 < c0);

#ifdef _USEMPI_
	MPI_Allreduce(MPI_IN_PLACE, &error, 1, MPI_INT, MPI_MAX, comm);
#endif

	if (error)
	    goto measure;

	tts += t1.tv_sec - t0.tv_sec
		+ 1e-6 * (t1.tv_usec - t0.tv_usec);

	cc += c1 - c0;
    }

    if (!r)
	printf("averaging over %d processes...\n", rn);

    tts /= rn;
    cc /= rn;

#ifdef _USEMPI_
    MPI_Allreduce(MPI_IN_PLACE, &cc, 1, MPI_LONG_DOUBLE, MPI_SUM, comm);
    MPI_Allreduce(MPI_IN_PLACE, &tts, 1, MPI_LONG_DOUBLE, MPI_SUM, comm);
#endif

    tn *= rep;

    if (!r)
	printf("TTS: %.3f s %e s BW: %.4f GB/s %.3f B/C %e C\n",
	       (double)tts, (double)tts / tn,
	       (double)(rn * tn * sizeof(real) * yxn * 1e-9 / tts),
	       (double)(tn * sizeof(real) * (long double)yxn / cc),
	       (double)(cc / (long double)tn)
	    );

    free(out);
    free(in);

#ifdef _USEMPI_
    MPI_Finalize();
#endif

    return EXIT_SUCCESS;
}
