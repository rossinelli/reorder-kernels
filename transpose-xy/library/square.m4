divert(-1)
dnl iter:
dnl $1: stmt
dnl $2: var
dnl $3: value
define(`iter',dnl
`pushdef(`$2', `$3')dnl
$1
popdef(`$2')')
dnl foreach:
dnl stmt
dnl itervar
dnl iterval
dnl define(`foreach',
dnl debugmode(ta)
define(`foreach',dnl
`iter(`$1', $2, $3)dnl
ifelse(eval($# >= 4), 1, `foreach(`$1', `$2', shift(shift(shift($@))))')')

dnl first:
dnl list
define(`first', `$1')

divert(0)

typedef void (*K)(_REAL_ *);

define(`max', first(_LIST_))dnl
foreach(`ifelse(eval(max < i), 1, `define(`max', i)')dnl
void square_transpose_`'i`_'_REAL_`'(_REAL_ *);', i, _LIST_)dnl

static K kernels[eval(max + 1)];

#include <string.h>

define(`postfix', `ifelse(_REAL_, float, `f', `d')')
int square_transpose`'postfix`'(
     _REAL_ * in,
     const int n,
     _REAL_ * out )
{
	if (n > max) return 0;

	K k = kernels[n];

	if (!k)
	   return 0;

	_REAL_ * dst = in;

	if (out && out != in)
	{
	   memcpy(out, in, sizeof(_REAL_) * n * n);
	   dst = out;
	}

	k(dst);

	return 1;
}


static void __attribute__((constructor)) init(void)
{
	memset(kernels, 0, sizeof(kernels));
	foreach(`kernels[i] = square_transpose_`'i`_'_REAL_;', i, _LIST_)
}

undefine(`max')dnl
