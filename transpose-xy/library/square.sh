#!/usr/bin/env bash
set -e

rm -fr square
mkdir square

declare -A type2real
type2real[fp32]=float
type2real[fp64]=double

for real in fp32 fp64
do
    list=""
    for ((n=8; n <= 256; n+= 8))
    do
	objname=`printf "square-%d-$real.o" $n`
	name=`printf "square_transpose_%d_%s" $n ${type2real[$real]}`

	if true; then
	    echo "generating $name"
	    make -C ../ size=$n name=$name objname=$objname real=$real impl=$IMPL
            mv $objname square/.
	fi

	if [ ! -z "$list" ] ; then
	    list="$list, "
	fi
	list="$list$n"
    done
done

echo "captured sizes: <$list>"

cd square
m4 -D _REAL_=float -D _LIST_="$list" ../square.m4 > dispatchf.c
m4 -D _REAL_=double -D _LIST_="$list" ../square.m4 > dispatchd.c

$CC $CFLAGS -fPIC -c dispatchf.c
$CC $CFLAGS -fPIC -c dispatchd.c

cd ..

ar rcs libtranspose-square.a square/*.o



