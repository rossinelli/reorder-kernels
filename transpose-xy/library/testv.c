#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include <sys/time.h>

#include <common-util.h>
#include <posix-util.h>

#include "transpose-xy.h"

int main (
    const int argc,
    const char * const argv[] )
{
    if (argc != 4)
    {
	fprintf(stderr,
		"usage: %s <element-byte-count> <xsize> <ysize>\n",
		argv[0]);

	return EXIT_FAILURE;
    }

    const ptrdiff_t elsz = atoi(argv[1]);
    const ptrdiff_t xn = atoi(argv[2]);
    const ptrdiff_t yn = atoi(argv[3]);

    const ptrdiff_t yxn = yn * xn;

    void * const in = malloc(elsz * yxn);
    void * const out = malloc(elsz * yxn);

    for(ptrdiff_t y = 0; y < yn; ++y)
	for(ptrdiff_t x = 0; x < xn; ++x)
	{
	    uint8_t * const line = elsz * (x + xn * y) + (uint8_t *)in;

	    for(ptrdiff_t c = 0; c < elsz; ++c)
		line[c] = 0xff & (c + elsz * (x + xn * y));
	}

    transpose_xyv (in, yn, xn, elsz, out);

    for(ptrdiff_t x = 0; x < xn; ++x)
	for(ptrdiff_t y = 0; y < yn; ++y)
	{
	    const uint8_t * const line = elsz * (y + yn * x) + (uint8_t *)out;

	    for(ptrdiff_t c = 0; c < elsz; ++c)
	    {
		const uint8_t expected = 0xff & (c + elsz * (x + xn * y));

		CHECK(expected == line[c],
		      "error: %d != out[%d] at x: %zd, y: %zd\n",
		      expected, line[c], x, y);
	    }
	}

    printf("accuracy test passed.\n");

    struct timeval t0, t1;

    const int64_t c0 = rdtsc();
    POSIX_CHECK(0 == gettimeofday(&t0, NULL));

    const int tn = (int)((15000 * 1e6) / (yxn * elsz * 2));

    for(int t = 0; t < tn; ++t)
	transpose_xyv (in, yn, xn, elsz, out);

    POSIX_CHECK(0 == gettimeofday(&t1, NULL));
    const int64_t c1 = rdtsc();

    const double tts = t1.tv_sec - t0.tv_sec
	+ 1e-6 * (t1.tv_usec - t0.tv_usec);

    printf("TTS: %.3f s BW: %.4f GB/s %.3f B/C\n",
	   tts,
	   tn * elsz * yxn * 1e-9 / tts,
	   (double)(tn * elsz * (long double)yxn / (long double)(c1 - c0)));

    free(out);
    free(in);

    return EXIT_SUCCESS;
}
