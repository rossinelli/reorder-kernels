divert(-1)
include(../../share/util.m4)
divert(0)
`#'include <stddef.h>

forloop(m, 2, eval(_N_ - 1), `
static void to_aos`'m (
const REAL * const __restrict__ in,
const ptrdiff_t n,
REAL * const __restrict__ out)
{
forloop(i, 0, eval(m - 1), `dnl
const REAL * const __restrict__ in`'i' = in + n * i; )

for (ptrdiff_t src = 0; src < n; ++src)
{
	const ptrdiff_t obase = src * m;

	forloop(i, 0, eval(m - 1), `dnl
	out[obase + i] = in`'i[src];')
}
}
')

forloop(m, 2, _N_, `
static void to_soa`'m (
const REAL * const __restrict__ in,
const ptrdiff_t n,
REAL * const __restrict__ out)
{
dnl forloop(i, 0, eval(m - 1), `dnl
dnl REAL * const __restrict__ out`'i = out + n * i;
dnl ')
dnl for (ptrdiff_t dst = 0; dst < n; ++dst)
dnl {
dnl 	const ptrdiff_t ibase = dst * m;
dnl 
dnl 	forloop(i, 0, eval(m - 1), `dnl
dnl 	out`'i[dst] = in[ibase + i];
dnl 	')
dnl }
        for (ptrdiff_t c = 0; c < m; ++c)
    for (ptrdiff_t i = 0; i < n; ++i)
        out[i + n * c] = in[c + m * i];

}
')

static __typeof__(to_aos2) * toAOS[_N_] = {NULL, NULL,
forloop(m, 2, eval(_N_ - 1),`dnl
ifelse(m,2,,`, ')to_aos`'m') };

static __typeof__(to_soa2) * toSOA[_N_] = {NULL, NULL,
forloop(m, 2, eval(_N_ - 1),`dnl
ifelse(m,2,,`, ')to_soa`'m') };

`#'include <assert.h>
`#'include <string.h>
void NAME (
     const REAL * const __restrict__ in,
    const int yn_in,
    const int xn_in,
    REAL * const __restrict__ out )
{
	if (1 == yn_in || 1 == xn_in)
	memcpy(out, in, sizeof(REAL) * (ptrdiff_t)xn_in * (ptrdiff_t)yn_in);
	else
	if (yn_in <= xn_in)
{
	assert(yn_in >= 2 && yn_in < _N_);
	   toAOS[yn_in](in, xn_in, out);
}
	else
{
	assert(xn_in >= 2 && xn_in < _N_);
	   toSOA[xn_in](in, yn_in, out);
}
}
