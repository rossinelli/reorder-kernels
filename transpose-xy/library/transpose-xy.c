#define CAT(x, y) x ## y
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#include <stddef.h>

#ifdef _FP32_
typedef float real;
#define DEFF(x) CAT(x, f)

void panel_transpose32 (
		const ptrdiff_t panelsize,
		const ptrdiff_t istride,
		const ptrdiff_t ostride,
		const float * const restrict src,
		float * const restrict dst );

#else
typedef double real;
#define DEFF(x) CAT(x, d)
#endif

enum { TILE = _TILE_ } ;

static void cpy (
		const real * const __restrict__ in,
		const int n,
		real * const __restrict__ out)
{
#pragma GCC unroll (4)
	for(int i = 0; i < n; ++i)
		out[i] = in[i];
}

#define KER DEFF(xyt)
void KER (real * __restrict__ const data);

#define TINY DEFF(tiny)
void TINY(
		const real * const __restrict__ in,
		const int yn_in,
		const int xn_in,
		real * const __restrict__ out );

#define _TILE_WORK_()							\
{									\
	/*copy to block */						\
	{								\
		const real * const __restrict__ src =			\
		in + xt + xn_in * (size_t)yt;				\
		\
		for(int line = 0; line < ytn; ++line)			\
		cpy(src + xn_in * (size_t)line, xtn, &block[line][0]);	\
	}								\
	\
	KER(&block[0][0]);						\
	\
	/* copy from block */						\
	{								\
		real * const __restrict__ dst =				\
		out + yt + yn_in * (size_t)xt;				\
		\
		for(int line = 0; line < xtn; ++line)			\
		cpy(&block[line][0], ytn, dst + yn_in * (size_t)line);	\
	}								\
}

#include <string.h>

int DEFF(square_transpose) (
		real * in,
		const int n,
		real * out );

#include <stdlib.h>

#include <common-util.h>

#ifdef _USEIPP_
#include <ippi.h>

static int REORDER_FALLBACK = 0;

static void __attribute__((constructor)) init (void)
{
	READENV(REORDER_FALLBACK, atoi);

}
#endif

__attribute__((visibility("default")))
void DEFF(transpose_xy) (
		const real * const __restrict__ in,
		const int yn_in,
		const int xn_in,
		real * __restrict__ out )
{
	const int isinplace = NULL == out || in == out;

#ifdef _USEIPP_
	if (!REORDER_FALLBACK)
	{
		/* is square */
		const int isq = xn_in == yn_in;

		if (isinplace && !isq)
			POSIX_CHECK(out = malloc(yn_in * xn_in * sizeof(real)));

		if (isinplace && isq)
#ifdef _FP32_
			ippiTranspose_32f_C1IR
#else
				ippiTranspose_16s_C4IR
#endif
				((void *)in, xn_in * sizeof(real), (IppiSize){ .width = xn_in, .height = yn_in});
		else
#ifdef _FP32_
			ippiTranspose_32f_C1R
#else
				ippiTranspose_16s_C4R
#endif
				((void *)in, xn_in * sizeof(real), (void *)out, yn_in * sizeof(real), (IppiSize){ .width = xn_in, .height = yn_in});

		if (isinplace && !isq)
		{
			memcpy((real *)in, out, yn_in * xn_in * sizeof(real));
			free(out);
		}

		return;
	}
#endif /* defined(_USEIPP_) */

	/* handling of square transpositions */
	if (yn_in == xn_in)
	{
		const int success = DEFF(square_transpose)
			((real *)in, yn_in, out);

		if (success)
			return;

		real * __restrict__ inplace = (real *)in;

		if (out && out != in)
		{
			inplace = out;
			memcpy(out, in, sizeof(real) * xn_in * xn_in);
		}

		for(int yt = 0; yt < yn_in; yt += TILE)
		{
			const int ytn =  MIN(TILE, yn_in - yt);

			for(int xt = 0; xt <= yt; xt += TILE)
			{
				const int xtn = MIN(TILE, xn_in - xt);

				real __attribute__((aligned(64))) block0[TILE][TILE];
				real __attribute__((aligned(64))) block1[TILE][TILE];

				/* copy to block0 */
				{
					const real * const __restrict__ src =
						inplace + xt + xn_in * (size_t)yt;

					for(int line = 0; line < ytn; ++line)
						cpy(src + xn_in * (size_t)line, xtn, &block0[line][0]);
				}

				/* copy to block1 */
				{
					const real * const __restrict__ src =
						inplace + yt + xn_in * (size_t)xt;

					for(int line = 0; line < xtn; ++line)
						cpy(src + xn_in * (size_t)line, ytn, &block1[line][0]);
				}

				KER(&block0[0][0]);
				KER(&block1[0][0]);

				/* copy from block1 */
				{
					real * const __restrict__ dst =
						inplace + xt + xn_in * (size_t)yt;

					for(int line = 0; line < ytn; ++line)
						cpy(&block1[line][0], xtn, dst + xn_in * (size_t)line);
				}

				/* copy from block0 */
				{
					real * const __restrict__ dst =
						inplace + yt + xn_in * (size_t)xt;

					for(int line = 0; line < xtn; ++line)
						cpy(&block0[line][0], ytn, dst + xn_in * (size_t)line);
				}
			}
		}

		return;
	}

#ifdef _FP32_
	enum { THRESHOLD_TINY = 4 * sizeof(real) };
#else
	enum { THRESHOLD_TINY = _TILE_ };
#endif


	if (isinplace)
		POSIX_CHECK(out = malloc(yn_in * xn_in * sizeof(real)));

	if (yn_in < THRESHOLD_TINY || xn_in < THRESHOLD_TINY)
		TINY(in, yn_in, xn_in, out);
	else
	{
#ifdef _FP32_

		panel_transpose32(xn_in, xn_in, yn_in, in, out);
#else
		real __attribute__((aligned(64))) block[TILE][TILE];

		if (yn_in >= xn_in)
		{
			for(int yt = 0; yt < yn_in; yt += TILE)
			{
				const int ytn =  MIN(TILE, yn_in - yt);

				for(int xt = 0; xt < xn_in; xt += TILE)
				{
					const int xtn = MIN(TILE, xn_in - xt);

					_TILE_WORK_();
				}
			}
		}
		else
			for(int xt = 0; xt < xn_in; xt += TILE)
			{
				const int xtn = MIN(TILE, xn_in - xt);

				for(int yt = 0; yt < yn_in; yt += TILE)
				{
					const int ytn =  MIN(TILE, yn_in - yt);

					_TILE_WORK_();
				}
			}
#endif
	}

	if (isinplace)
	{
		memcpy((real *)in, out, yn_in * xn_in * sizeof(real));
		free(out);
	}
}

#ifdef _FP32_
void transpose_xyf_panel (
		const void * const __restrict__ in,
		const int yn_in,
		const int xn_in,
		const int panelsize,
		void * const __restrict__ out )
{
	panel_transpose32(panelsize, xn_in, yn_in, in, out);
}
#endif
