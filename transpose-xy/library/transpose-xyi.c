#include <stdint.h>
#include <stddef.h>

#ifdef _I16_
typedef int16_t small;
typedef int32_t large;
void xytd (double * restrict const data);
#define KCALL xytd
#define KDEF transpose_xys
#define KTINY tinys
#else
typedef int8_t small;
typedef int16_t large;
void xytf (float * restrict const data);
#define KCALL xytf
#define KDEF transpose_xyc
#define KTINY tinyc
#endif

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

enum
{
	TILE = _TILE_,
	TILE2 = 2 * TILE,
};

void KTINY(
	const small * const restrict in,
	const int yn_in,
	const int xn_in,
	small * const restrict out );

static void mux2 (
	const small * const restrict _in0,
	const small * const restrict _in1,
	const int h,
	small * const restrict out)
{
	const small * const restrict in0 = __builtin_assume_aligned (_in0, sizeof(small));
	const small * const restrict in1 = __builtin_assume_aligned (_in1, sizeof(small));

	const int q = h / 2;

	{
		const large * const restrict i0 = (large *)in0;
		const large * const restrict i1 = (large *)in1;

		large * const restrict o = (large *)out;

#pragma GCC unroll (8)
		for (int i = 0; i < q; ++i)
		{
			o[0 + 2 * i] = i0[i];
			o[1 + 2 * i] = i1[i];
		}
	}

	const int q2 = q * 2;
	const int q4 = q * 4;

	if (q2 < h)
	{
		out[0 + q4] = in0[q2];
		out[2 + q4] = in1[q2];
	}
}

static void mux1 (
	const small * const restrict in,
	const int h,
	small * const restrict out)
{
	const int q = h / 2;

	{
		const large * const restrict i0 = (large *)in;

		large * const restrict o = (large *)out;

		for (int i = 0; i < q; ++i)
			o[0 + 2 * i] = i0[i];
	}

	const int q2 = q * 2;
	const int q4 = q * 4;

	if (q2 < h)
		out[0 + q4] = in[q2];
}

static void demux2 (
	const small * const restrict in,
	const int h,
	small * const restrict out0,
	small * const restrict out1 )
{
#pragma GCC unroll (8)
	for (int i = 0; i < h; ++i)
		out0[i] = in[0 + 2 * i];

#pragma GCC unroll (8)
	for (int i = 0; i < h; ++i)
		out1[i] = in[1 + 2 * i];
}

static void demux1 (
	const small * const restrict in,
	const int h,
	small * const restrict out )
{
	for (int i = 0; i < h; ++i)
		out[i] = in[0 + 2 * i];
}

#include <stdlib.h>
#include <string.h>

#include <posix-util.h>

#ifdef _USEIPP_
#include <ippi.h>

static int REORDER_FALLBACK = 0;

#include <common-util.h>

static void __attribute__((constructor)) init (void)
{
	READENV(REORDER_FALLBACK, atoi);

}
#endif

__attribute__((visibility("default")))
void KDEF (
	const small * const restrict in,
	const int yn_in,
	const int xn_in,
	small * restrict out )
{
	const int isinplace = NULL == out || in == out;

#ifdef _USEIPP_
	if (!REORDER_FALLBACK)
	{
		/* is square */
		const int isq = xn_in == yn_in;

		if (isinplace && !isq)
			POSIX_CHECK(out = malloc(yn_in * xn_in * sizeof(small)));

		if (isinplace && isq)
#ifdef _I16_
			ippiTranspose_16u_C1IR
#else
				ippiTranspose_8u_C1IR
#endif
				((void *)in, xn_in * sizeof(small), (IppiSize){ .width = xn_in, .height = yn_in});
		else
#ifdef _I16_
			ippiTranspose_16u_C1R
#else
				ippiTranspose_8u_C1R
#endif
				((void *)in, xn_in * sizeof(small), (void *)out, yn_in * sizeof(small), (IppiSize){ .width = xn_in, .height = yn_in});

		if (isinplace && !isq)
		{
			memcpy((small *)in, out, yn_in * xn_in * sizeof(small));
			free(out);
		}

		return;
	}
#endif /* defined(_USEIPP_) */

	if (isinplace)
		POSIX_CHECK(out = malloc(yn_in * xn_in * sizeof(small)));

	if (yn_in < TILE2 || xn_in < TILE2)
		KTINY(in, yn_in, xn_in, out);
	else
	{
		small __attribute__((aligned(64))) block[TILE][TILE][4];

		for(int xt = 0; xt < xn_in; xt += TILE2)
		{
			const int xtn = MIN(TILE2, xn_in - xt);

			for(int yt = 0; yt < yn_in; yt += TILE2)
			{
				const int ytn =  MIN(TILE2, yn_in - yt);

				/*copy to block */
				{
					const small * const restrict src =
						in + xt + xn_in * (size_t)yt;

					const int ytn2 = ytn & ~1;

					for(int line = 0; line < ytn2; line += 2)
					{
						mux2(src + xn_in * (size_t)(line + 0),
							 src + xn_in * (size_t)(line + 1),
							 xtn, &block[line >> 1][0][0]);
					}

					if (ytn2 < ytn)
						mux1(src + xn_in * (size_t)ytn2,
							 xtn, &block[ytn2 >> 1][0][0]);
				}

				KCALL((void *)&block[0][0]);

				/* copy from block */
				{
					small * const restrict dst =
						out + yt + yn_in * (size_t)xt;

					const int xtn2 = xtn & ~1;

					for(int line = 0; line < xtn2; line += 2)
						demux2(&block[line >> 1][0][0], ytn,
							   dst + yn_in * (size_t)(line + 0),
							   dst + yn_in * (size_t)(line + 1));

					if (xtn2 < xtn)
						demux1(&block[xtn2 >> 1][0][0], ytn,
							   dst + yn_in * (size_t)xtn2);
				}
			}
		}
	}

	if (isinplace)
	{
		memcpy((small *)in, out, yn_in * xn_in * sizeof(small));

		free(out);
	}
}
