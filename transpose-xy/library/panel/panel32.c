#include <stddef.h>

void panel_transpose32 (
    const ptrdiff_t panelsize,
    const ptrdiff_t istride,
    const ptrdiff_t ostride,
    const float * const restrict src,
    float * const restrict dst )
{
    const ptrdiff_t xc_nice4 = panelsize & ~3;
    const ptrdiff_t yc_nice4 = ostride & ~3;

    for (ptrdiff_t x = 0; x < xc_nice4; x += 4)
    {
		for (ptrdiff_t y = 0; y < yc_nice4; y += 4)
		{
			const float * const restrict in = src + x + istride * y;

			float * const restrict out = dst + y + ostride * x;
			
			for (ptrdiff_t i = 0; i < 4; ++i)
				for (ptrdiff_t j = 0; j < 4; ++j)
					out[i + ostride * j] = in[j + istride * i];
		}
		
		for (ptrdiff_t y = yc_nice4; y < ostride; ++y)
			for (ptrdiff_t lx = 0; lx < 4; ++lx)
				dst[y + ostride * (x + lx)] = src[(x + lx) + istride * y];

		for (ptrdiff_t x = xc_nice4; x < panelsize; ++x)
			for (ptrdiff_t y = 0; y < ostride; ++y)
				dst[y + ostride * x] = src[x + istride * y];
	}
}
