#include <stddef.h>
#include <immintrin.h>

void panel_transpose32 (
	const ptrdiff_t panelsize,
	const ptrdiff_t istride,
	const ptrdiff_t ostride,
	const float * const restrict src,
	float * const restrict dst )
{
	const ptrdiff_t xc_nice8 = panelsize & ~7;
	const ptrdiff_t yc_nice8 = ostride & ~7;
	const ptrdiff_t yc_nice4 = ostride & ~3;

	const __m512i lo = _mm512_set_epi32 (
		11 + 16, 11 + 0, 10 + 16, 10 + 0,
		3 + 16, 3 + 0, 2 + 16, 2 + 0,
		9 + 16, 9 + 0, 8 + 16, 8 + 0,
		1 + 16, 1 + 0, 0 + 16, 0 + 0 );

	const __m512i hi = lo + _mm512_set1_epi32(4);

	for (ptrdiff_t x = 0; x < xc_nice8; x += 8)
	{
		for (ptrdiff_t y = 0; y < yc_nice8; y += 8)
		{
			const float * const restrict in = src + x + istride * y;

			const __m512 q_0 = _mm512_castps256_ps512
				(_mm256_loadu_ps(in + istride * 0));
			const __m512 q_1 = _mm512_castps256_ps512
				(_mm256_loadu_ps(in + istride * 1));

			const __m512 r_0 = _mm512_insertf32x8
				(q_0, _mm256_loadu_ps(in + istride * 4), 1);
			const __m512 r_1 = _mm512_insertf32x8
				(q_1, _mm256_loadu_ps(in + istride * 5), 1);

			const __m512 q_2 = _mm512_castps256_ps512
				(_mm256_loadu_ps(in + istride * 2));
			const __m512 q_3 = _mm512_castps256_ps512
				(_mm256_loadu_ps(in + istride * 3));

			const __m512 r_2 = _mm512_insertf32x8
				(q_2, _mm256_loadu_ps(in + istride * 6), 1);
			const __m512 r_3 = _mm512_insertf32x8
				(q_3, _mm256_loadu_ps(in + istride * 7), 1);

			float * const restrict out = dst + y + ostride * x;

			{
				const __m512 t_0 = _mm512_permutex2var_ps(r_0, lo, r_2);
				const __m512 t_1 = _mm512_permutex2var_ps(r_1, lo, r_3);

				const __m512 u_0 = _mm512_unpacklo_ps(t_0, t_1);
				const __m512 u_1 = _mm512_unpackhi_ps(t_0, t_1);

				_mm256_storeu_ps(out + ostride * 0, _mm512_extractf32x8_ps(u_0, 0));
				_mm256_storeu_ps(out + ostride * 1, _mm512_extractf32x8_ps(u_1, 0));
				_mm256_storeu_ps(out + ostride * 2, _mm512_extractf32x8_ps(u_0, 1));
				_mm256_storeu_ps(out + ostride * 3, _mm512_extractf32x8_ps(u_1, 1));
			}

			{
				const __m512 t_2 = _mm512_permutex2var_ps(r_0, hi, r_2);
				const __m512 t_3 = _mm512_permutex2var_ps(r_1, hi, r_3);

				const __m512 u_2 = _mm512_unpacklo_ps(t_2, t_3);
				const __m512 u_3 = _mm512_unpackhi_ps(t_2, t_3);

				_mm256_storeu_ps(out + ostride * 4, _mm512_extractf32x8_ps(u_2, 0));
				_mm256_storeu_ps(out + ostride * 5, _mm512_extractf32x8_ps(u_3, 0));
				_mm256_storeu_ps(out + ostride * 6, _mm512_extractf32x8_ps(u_2, 1));
				_mm256_storeu_ps(out + ostride * 7, _mm512_extractf32x8_ps(u_3, 1));
			}
		}

		if (yc_nice4 > yc_nice8)
		{
			const ptrdiff_t y = yc_nice8;

			const float * const restrict in = src + x + istride * y;

			float * const restrict out = dst + y + ostride * x;

			const __m256 r_0 = _mm256_loadu_ps(in + istride * 0);
			const __m256 r_1 = _mm256_loadu_ps(in + istride * 1);
			const __m256 r_2 = _mm256_loadu_ps(in + istride * 2);
			const __m256 r_3 = _mm256_loadu_ps(in + istride * 3);

			const __m256 s_0 = _mm256_unpacklo_ps(r_0, r_2);
			const __m256 s_1 = _mm256_unpacklo_ps(r_1, r_3);
			const __m256 s_2 = _mm256_unpackhi_ps(r_0, r_2);
			const __m256 s_3 = _mm256_unpackhi_ps(r_1, r_3);

			const __m256 t_0 = _mm256_unpacklo_ps(s_0, s_1);
			const __m256 t_1 = _mm256_unpackhi_ps(s_0, s_1);
			const __m256 t_2 = _mm256_unpacklo_ps(s_2, s_3);
			const __m256 t_3 = _mm256_unpackhi_ps(s_2, s_3);

			_mm_storeu_ps(out + ostride * 0, _mm256_extractf32x4_ps(t_0, 0));
			_mm_storeu_ps(out + ostride * 1, _mm256_extractf32x4_ps(t_1, 0));
			_mm_storeu_ps(out + ostride * 2, _mm256_extractf32x4_ps(t_2, 0));
			_mm_storeu_ps(out + ostride * 3, _mm256_extractf32x4_ps(t_3, 0));
			_mm_storeu_ps(out + ostride * 4, _mm256_extractf32x4_ps(t_0, 1));
			_mm_storeu_ps(out + ostride * 5, _mm256_extractf32x4_ps(t_1, 1));
			_mm_storeu_ps(out + ostride * 6, _mm256_extractf32x4_ps(t_2, 1));
			_mm_storeu_ps(out + ostride * 7, _mm256_extractf32x4_ps(t_3, 1));
		}

		for (ptrdiff_t y = yc_nice4; y < ostride; ++y)
			for (ptrdiff_t lx = 0; lx < 8; ++lx)
				dst[y + ostride * (x + lx)] = src[(x + lx) + istride * y];
	}

	for (ptrdiff_t x = xc_nice8; x < panelsize; ++x)
		for (ptrdiff_t y = 0; y < ostride; ++y)
			dst[y + ostride * x] = src[x + istride * y];
}
