#include <stddef.h>
#include <immintrin.h>

void panel_transpose32 (
    const ptrdiff_t panelsize,
    const ptrdiff_t istride,
    const ptrdiff_t ostride,
    const float * const restrict src,
    float * const restrict dst )
{
    const ptrdiff_t xc_nice4 = panelsize & ~3;

    const ptrdiff_t yc_nice8 = ostride & ~7;
    const ptrdiff_t yc_nice4 = ostride & ~3;

    for (ptrdiff_t x = 0; x < xc_nice4; x += 4)
    {
	for (ptrdiff_t y = 0; y < yc_nice8; y += 8)
	{
	    const float * const restrict in = src + x + istride * y;

	    float * const restrict out = dst + y + ostride * x;

	    const __m256 q_0 = _mm256_castps128_ps256
		(_mm_loadu_ps(in + istride * 0));
	    const __m256 q_1 = _mm256_castps128_ps256
		(_mm_loadu_ps(in + istride * 1));
	    const __m256 q_2 = _mm256_castps128_ps256
		(_mm_loadu_ps(in + istride * 2));
	    const __m256 q_3 = _mm256_castps128_ps256
		(_mm_loadu_ps(in + istride * 3));

	    const __m256 r_0 = _mm256_insertf128_ps
		(q_0, _mm_loadu_ps(in + istride * 4), 1);
	    const __m256 r_1 = _mm256_insertf128_ps
		(q_1, _mm_loadu_ps(in + istride * 5), 1);
	    const __m256 r_2 = _mm256_insertf128_ps
		(q_2, _mm_loadu_ps(in + istride * 6), 1);
	    const __m256 r_3 = _mm256_insertf128_ps
		(q_3, _mm_loadu_ps(in + istride * 7), 1);

	    {
		const __m256 s_0 = _mm256_unpacklo_ps(r_0, r_2);
		const __m256 s_1 = _mm256_unpacklo_ps(r_1, r_3);

		_mm256_storeu_ps(out + ostride * 0, _mm256_unpacklo_ps(s_0, s_1));
		_mm256_storeu_ps(out + ostride * 1, _mm256_unpackhi_ps(s_0, s_1));
	    }

	    {
		const __m256 s_0 = _mm256_unpackhi_ps(r_0, r_2);
		const __m256 s_1 = _mm256_unpackhi_ps(r_1, r_3);

		_mm256_storeu_ps(out + ostride * 2, _mm256_unpacklo_ps(s_0, s_1));
		_mm256_storeu_ps(out + ostride * 3, _mm256_unpackhi_ps(s_0, s_1));
	    }
	}

	if (yc_nice4 > yc_nice8)
	{
	    const ptrdiff_t y = yc_nice8;

	    const float * const restrict in = src + x + istride * y;

	    float * const restrict out = dst + y + ostride * x;

	    const __m128 r_0 = _mm_loadu_ps(in + istride * 0);
	    const __m128 r_1 = _mm_loadu_ps(in + istride * 1);
	    const __m128 r_2 = _mm_loadu_ps(in + istride * 2);
	    const __m128 r_3 = _mm_loadu_ps(in + istride * 3);

	    {
		const __m128 s_0 = _mm_unpacklo_ps(r_0, r_2);
		const __m128 s_1 = _mm_unpacklo_ps(r_1, r_3);

		_mm_storeu_ps(out + ostride * 0, _mm_unpacklo_ps(s_0, s_1));
		_mm_storeu_ps(out + ostride * 1, _mm_unpackhi_ps(s_0, s_1));
	    }

	    {
		const __m128 s_0 = _mm_unpackhi_ps(r_0, r_2);
		const __m128 s_1 = _mm_unpackhi_ps(r_1, r_3);

		_mm_storeu_ps(out + ostride * 2, _mm_unpacklo_ps(s_0, s_1));
		_mm_storeu_ps(out + ostride * 3, _mm_unpackhi_ps(s_0, s_1));
	    }
	}

	for (ptrdiff_t y = yc_nice4; y < ostride; ++y)
	    for (ptrdiff_t lx = 0; lx < 4; ++lx)
		dst[y + ostride * (x + lx)] = src[(x + lx) + istride * y];
    }

    for (ptrdiff_t x = xc_nice4; x < panelsize; ++x)
	for (ptrdiff_t y = 0; y < ostride; ++y)
	    dst[y + ostride * x] = src[x + istride * y];
}
