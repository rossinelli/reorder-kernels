#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>

#ifdef _USEMPI_
#include <mpi.h>
#define _BARRIER_() MPI_Barrier(comm)
#else
#define _BARRIER_()
#endif

#include <sys/time.h>

#include <sched.h>

double rdtss(void)
{
    struct timeval tv;

    gettimeofday(&tv, NULL);

    return tv.tv_sec + 1e-6 * tv.tv_usec;
}

#if 1 //_WIN32
int64_t get_cycles(void)
{
    unsigned int lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}
#else
int64_t get_cycles(void)
{
    unsigned a, d, c;

    c = (1<<30)+1;
    __asm__ volatile("rdpmc" : "=a" (a), "=d" (d) : "c" (c));

    return ((size_t)a) | (((size_t)d) << 32ull);
}
#endif
/*uint64_t rdtsc(void)
  {
  unsigned int lo, hi;
  __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
  return ((uint64_t)hi << 32) | lo;
  }*/

enum { N = _N_, N2 = N * N };

#ifdef _FP32_
typedef float real;
#else
typedef double real;
#endif

void transpose_xy(real * __restrict__ const data);


real __attribute__((aligned(64))) data[N2];

int main(void)
{
#ifdef _USEMPI_
    MPI_Init(NULL, NULL);
    MPI_Comm comm = MPI_COMM_WORLD;

    int r, rn;
    MPI_Comm_rank(comm, &r);
    MPI_Comm_size(comm, &rn);
#else
    const int r = 0;
    const int rn = 1;
#endif

#ifndef _WIN32
    {
	/* pin the process to make sure rdpmc values are consistent */
	cpu_set_t cpu_set;
	CPU_ZERO(&cpu_set);
	CPU_SET(r, &cpu_set);
	if (sched_setaffinity(0, sizeof(cpu_set), &cpu_set) < 0)
	    fprintf(stderr, "cannot set cpu affinity\n");
    }
#endif

    int i, j;

    for (i = 0; i < N; ++i)
	for(j = 0; j < N; ++j)
	    data[j + N * i] = j + N * i;

    transpose_xy(data);

    for (i = 0; i < N; ++i)
	for(j = 0; j < N; ++j)
	{
	    real expected = i + N * j;

	    if (data[j + N * i] != expected)
	    {
		fprintf(stderr, "error: consistency check failed.\n");

		return EXIT_FAILURE;
	    }
	}

    if (!r)
	printf("consistency check passed (address: 0x%zx)\n",
	       (ptrdiff_t)data);

    int rep = 1000;

    if (getenv("REP"))
	rep = atoi(getenv("REP"));

    int tn = 10000;

    if (getenv("TN"))
	tn = atoi(getenv("TN"));

    long double tts = 0, cc = 0;

    for (int rr = 0; rr < rep; ++rr)
    {
    measure:

	_BARRIER_();
	const uint64_t c0 = get_cycles();
	const double t0 = rdtss();

	for(i = 0; i < tn; ++i)
	    transpose_xy(data);

	const double t1 = rdtss();
	const uint64_t c1 = get_cycles();
	_BARRIER_();

	int error = (c1 < c0);

#ifdef _USEMPI_
	MPI_Allreduce(MPI_IN_PLACE, &error, 1, MPI_INT, MPI_MAX, comm);
#endif

	if (error)
	    goto measure;

	cc += (long double)(c1 - c0);
	tts += (long double)(t1 - t0);
    }

    if (!r)
	printf("averaging over %d processes...\n", rn);

    tts /= rn;
    cc /= rn;

#ifdef _USEMPI_
    MPI_Allreduce(MPI_IN_PLACE, &cc, 1, MPI_LONG_DOUBLE, MPI_SUM, comm);
    MPI_Allreduce(MPI_IN_PLACE, &tts, 1, MPI_LONG_DOUBLE, MPI_SUM, comm);
#endif

    tn *= rep;

    if (!r)
	printf("THROUGHPUT: %.2f B/C  %.3f GB/s\n",
	       (double)(tn * sizeof(real) * N2 / cc),
	       (double)(1e-9 * rn * (long double)tn * sizeof(real) * N2 / tts)
	    );

#ifdef _USEMPI_
    MPI_Finalize();
#endif
    return EXIT_SUCCESS;
}
