divert(-1)
include(../share/util.m4)
divert(0)


ifdef(`_AVX512_', `
ifelse(real, float,
`#include"../share/macros-avx512-float.h"
enum { N = _N_, NNICE = N & ~15, N2 = N * N };
define(_VL_, 16)',
real, double,
`#include "../share/macros-avx512-double.h"
enum { N = _N_, NNICE = N & ~7, N2 = N * N };
define(_VL_, 8)')')
ifdef(`_AVX_', `
ifelse(real, float,
`#include"../share/macros-avx-float.h"
enum { N = _N_, NNICE = N & ~7, N2 = N * N };
define(_VL_, 8)',
real, double,
`#include "../share/macros-avx-double.h"
enum { N = _N_, NNICE = N & ~3, N2 = N * N };
define(_VL_, 4)')',
ifdef(`_AVX512_',, ``enum { N = _N_, `NNICE' = N & ~3, N2 = N * N};
typedef real `real';
define(_VL_, 4)''))

ifdef(`_AVX512_', `', `ifdef(`_AVX_',`',`#include "macros.h"')')

divert(-1)
define(`VLM1', `eval(_VL_ - 1)')
define(`_NNICE_', `eval(_N_ & ~VLM1)')
define(`_NM1_', `eval(_N_ - 1)')
divert(0)

/* VL is _VL_ mask is VLM1 
   N - 1 is _NM1_
*/
void transpose_xy(real * __restrict__ const data)
{
    real __attribute__((aligned(64))) tmp[TILE2];

    for(int y = 0; y < NNICE; y += TILE)
    {
	{
	    real * __restrict__ const r0 = data + y + N * y;

	    LOAD_TRANSPOSED(r0, tmp, N);

	    WRITE(tmp, r0, N);
	}

	real * __restrict__ const base0 = data + N * y;
	real * __restrict__ const base1 = data + y;

	for(int x = y + TILE; x < NNICE; x += TILE)
	{
	    real * __restrict__ const r0 = base0 + x;
	    real * __restrict__ const r1 = base1 + N * x;

	    LOAD_TRANSPOSED(r0, tmp, N);
	    LTW(r1, r0, N);
	    WRITE(tmp, r1, N);
	}
    }

    ifelse(eval(_NNICE_ <= _NM1_), 1, `
    /* handling entries from _NNICE_ to _NM1_ */
    LUNROLL(y, _NNICE_, _NM1_, `
        for(int x = 0; x < y; ++x)
        {
            const real t0 = data[x + N * y];
            const real t1 = data[y + N * x];

            data[x + N * y] = t1;
            data[y + N * x] = t0;
        }
	')')
}
