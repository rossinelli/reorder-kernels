This subfolder generates and deploys object files with the following signature:

```
void aos8_from_soa (
       const float * __restrict__ const src, 
       float * __restrict__ const dst);

void aos8_to_soa (
       const float * __restrict__ const src, 
       float * __restrict__ const dst);
```

Remarks:

- The function names change according to the makefile variable "prefix".
- The aos8 pointer must be *32-byte aligned*.
- For sizes that are multiple of 8, all arguments must be 32-byte aligned. 
- Input and output *cannot* be aliased.