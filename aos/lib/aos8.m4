divert(-1)
define(`forloop',
       `pushdef(`$1', `$2')_forloop(`$1', `$2', `$3', `$4')popdef(`$1')')

define(`_forloop',
       `$4`'ifelse($1, `$3', ,
		   `define(`$1', incr($1))_forloop(`$1', `$2', `$3', `$4')')')

define(LUNROLL, `forloop($1, $2, $3,`$4')')
divert(0)

define(`CAT', `$1_$2')
#include <immintrin.h>

void aos8_from_soa (
const float * __restrict__ const src,
const int n,
float * __restrict__ const dst)
{
	const int nice = n & ~3;

#pragma GCC unroll (2)
	for(int base = 0; base < nice; base += 4)
	{
		LUNROLL(c, 0, 3, `dnl
		const __m256 CAT(row, c) = _mm256_castps128_ps256(_mm_loadu_ps(&src[base + n * c]));
		')dnl

		LUNROLL(c, 0, 3, `dnl
		const __m256 CAT(v, c) = _mm256_insertf128_ps(CAT(row, c), _mm_loadu_ps(&src[base + n * (4 + c)]), 1);
		')dnl

		const int dstbase = 8 * base;

		{
			const __m256 r0 = _mm256_unpacklo_ps(v_0, v_2);
			const __m256 r1 = _mm256_unpacklo_ps(v_1, v_3);

			_mm256_store_ps(&dst[0 + dstbase], _mm256_unpacklo_ps(r0, r1));
			_mm256_store_ps(&dst[8 + dstbase], _mm256_unpackhi_ps(r0, r1));
		}

		{
			const __m256 r0 = _mm256_unpackhi_ps(v_0, v_2);
			const __m256 r1 = _mm256_unpackhi_ps(v_1, v_3);

			_mm256_store_ps(&dst[16 + dstbase], _mm256_unpacklo_ps(r0, r1));
			_mm256_store_ps(&dst[24 + dstbase], _mm256_unpackhi_ps(r0, r1));
		}
	}

	const int rem4 = n - nice;
	for(int i = 0; i < rem4; ++i)
	{
		const int dstbase = 8 * (i + nice);
		LUNROLL(c, 0, 7, `dnl
		dst[c + dstbase] = src[i + nice + n * c];
		')
	}
}

void aos8_to_soa (
const float * __restrict__ const src,
const int n,
float * __restrict__ const dst)
{
	const int nice = n & ~7;
	for(int base = 0; base < nice; base += 8)
	{
		const int srcbase = 8 * base;

		LUNROLL(pass, 0, 1, `
		{
		LUNROLL(c, 0, 3, `
		const __m256 CAT(q, c) = _mm256_castps128_ps256(_mm_load_ps(&src[srcbase + eval(4 * (pass + 2 * c))]));')
		LUNROLL(c, 0, 3, `
		const __m256 CAT(r, c) = _mm256_insertf128_ps(CAT(q, c), _mm_load_ps(&src[srcbase + eval(4 * (pass + 2 * (4 + c)))]), 1);')

		LUNROLL(t, 0, 1, `
		{
			const __m256 s_0 = ifelse(t,0,_mm256_unpacklo_ps,_mm256_unpackhi_ps)(CAT(r, 0), CAT(r, 2));
			const __m256 s_1 = ifelse(t,0,_mm256_unpacklo_ps,_mm256_unpackhi_ps)(CAT(r, 1), CAT(r, 3));

			_mm256_storeu_ps(&dst[base + n * eval(0 +  2 * (t + 2 * pass))], _mm256_unpacklo_ps(s_0, s_1));
			_mm256_storeu_ps(&dst[base + n * eval(1 +  2 * (t + 2 * pass))], _mm256_unpackhi_ps(s_0, s_1));
		}')
		}')
	}

	const int rem = n - nice;
	if (rem)
	{
	LUNROLL(c, 0, 7, `
	for(int i = 0; i < rem; ++i)
		dst[nice + n * c + i] = src[c + 8 * (nice + i)];
	')
	}
}
