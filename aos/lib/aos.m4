
void
`aos'_CN_`'_from_soa (
	const float *__restrict__ const src,
	const int n,
	float *__restrict__ const dst)
{
    for (int i = 0; i < n; ++i)
    for (int c = 0; c < _CN_; ++c)
        dst[c + _CN_ * i] = src[i + n * c];
}

void
`aos'_CN_`'_to_soa (
	const float *__restrict__ const src,
	const int n,
	float *__restrict__ const dst)
{
    for (int c = 0; c < _CN_; ++c)
    for (int i = 0; i < n; ++i)
        dst[i + n * c] = src[c + _CN_ * i];
}
