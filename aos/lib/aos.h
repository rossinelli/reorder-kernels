
void aos4_from_soa (
    const float *__restrict__ const src,
    const int n,
    float *__restrict__ const dst );

void aos4_to_soa (
    const float *__restrict__ const src,
    const int n,
    float *__restrict__ const dst );

void aos8_from_soa (
    const int *__restrict__ const src,
    const int n,
    int *__restrict__ const dst );

void aos8_to_soa (
    const int *__restrict__ const src,
    const int n,
    int *__restrict__ const dst );

void aos16_from_soa (
    const float *__restrict__ const src,
    const int n,
    float *__restrict__ const dst );

void aos16_to_soa (
    const float *__restrict__ const src,
    const int n,
    float *__restrict__ const dst );
