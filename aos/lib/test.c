#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <string.h>
#include <sys/time.h>

#include "aos.h"

#define READENV(x, op)				\
    do						\
    {						\
	if (getenv(#x))				\
	    x = op(getenv(#x));			\
    }						\
    while(0)

static inline uint64_t rdtsc()
{
    unsigned int lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

static inline double rdtss()
{
    struct timeval tv;

    gettimeofday(&tv, NULL);

    return tv.tv_sec + 1e-6 * tv.tv_usec;
}

int main (
    const int argc,
    const char * argv[])
{
    if (argc != 2)
    {
	fprintf(stderr,
		"usage: %s <length>\n",
		argv[0]);

	return EXIT_FAILURE;
    }

    const ptrdiff_t n = atoi(argv[1]);

    int * data = malloc(sizeof(int) * 8 * n);

    for (ptrdiff_t c = 0; c < 8; ++c)
	for (ptrdiff_t i = 0; i < n; ++i)
	    data[i + n * c] = i + n * c;

    int * tmp = malloc(sizeof(int) * 8 * n);

    aos8_from_soa(data, n, tmp);

    for (ptrdiff_t j = 0; j < n; ++j)
	for (ptrdiff_t i = 0; i < 8; ++i)
	    if (tmp[i + 8 * j] != data[j + n * i])
	    {
		fprintf(stderr, "error: aos8_from_soa: consistency check failed.\n");

		return EXIT_FAILURE;
	    }

    int * data0 = malloc(sizeof(int) * 8 * n);

    aos8_to_soa(tmp, n, data0);

    if (memcmp(data0, data, sizeof(int) * 8 * n))
    {
	fprintf(stderr, "error: aos8_to_soa: consistency check failed.\n");

	return EXIT_FAILURE;
    }

    printf("consistency check passed\n");

    ptrdiff_t RC = 1;
    READENV(RC, (ptrdiff_t)atof);

    const int ntimes = RC;

    const uint64_t t0 = rdtsc();

    for(ptrdiff_t t = 0; t < ntimes; ++t)
	aos8_to_soa(tmp, n, data0);

    const uint64_t t1 = rdtsc();

    for(ptrdiff_t t = 0; t < ntimes; ++t)
	aos8_from_soa(data, n, tmp);

    const uint64_t t2 = rdtsc();

    printf("THROUGHPUT:\n"
	   "TO-AOS   %.2f B/C\n"
	   "FROM-AOS %.2f B/C\n",
	   (double)ntimes * sizeof(float) * n * 8 / (double)(t1 - t0),
	   (double)ntimes * sizeof(float) * n * 8 / (double)(t2 - t1));

    return EXIT_SUCCESS;
}
