divert(-1)
include(../share/util.m4)

define(_NNICE_, eval(_N_ & ~7))
define(_REM_, eval(_N_ - _NNICE_))

define(CPPKERNELS, `
void aos8_from_soa (
       const float * __restrict__ const src, 
       float * __restrict__ const dst)
{
	for(int base = 0; base < _NNICE_; base += 8)
	{
		const int dstbase = 8 * base;

		LUNROLL(c, 0, 7, `dnl
		LUNROLL(xi, 0, 7, `dnl
		dst[eval(c + 8 * xi) + dstbase] = src[eval(xi + _N_ * c) + base];
		')')
	}

	for(int xi = 0; xi < _REM_; ++xi)
	{
		const int dstbase = 8 * xi;

		LUNROLL(c, 0, 7, `dnl
		dst[eval(c + 8 * _NNICE_) + dstbase] = src[eval(_NNICE_ + _N_ * c) + xi];
		')
	}
}

void aos8_to_soa (
       const float * __restrict__ const src, 
       float * __restrict__ const dst)
{
	float tmp[8][8];
	
	for(int base = 0; base < _NNICE_; base += 8)
	{
		const int srcbase = 8 * base;

		LUNROLL(c, 0, 7, `dnl
		LUNROLL(xi, 0, 7, `dnl
		dst[eval(xi + _N_ * c) + base] = src[eval(c + 8 * xi) + srcbase];
		')')
	}

	for(int xi = 0; xi < _REM_; ++xi)
	{
		const int srcbase = 8 * xi;

		LUNROLL(c, 0, 7, `dnl
		dst[eval(_NNICE_ + _N_ * c) + xi] = src[eval(c + 8 * _NNICE_) + srcbase];
		')
	}
}
') #end of CPPKERNELS

define(_NNICE4_, eval(_N_ & ~3))
define(_REM4_, eval(_N_ - _NNICE4_))

define(AVXKERNELS, `
#include "macros-avx-float.h"

void aos8_from_soa (
const float * __restrict__ const src, 
float * __restrict__ const dst)
{
	for(int base = 0; base < _NNICE4_; base += 4)
	{
		LUNROLL(c, 0, 3, `dnl
		const __m256 TMP(row, c) = _mm256_castps128_ps256(_mm_loadu_ps(&src[base + eval(_N_ * c)]));
		')dnl

		LUNROLL(c, 0, 3, `dnl
		const __m256 TMP(v, c) = _mm256_insertf128_ps(TMP(row, c), _mm_loadu_ps(&src[base + eval(_N_ * (4 + c))]), 1);
		')dnl

		const int dstbase = 8 * base;

		{
			const __m256 r0 = _mm256_unpacklo_ps(v_0, v_2);
			const __m256 r1 = _mm256_unpacklo_ps(v_1, v_3);

			_mm256_store_ps(&dst[0 + dstbase], _mm256_unpacklo_ps(r0, r1));
			_mm256_store_ps(&dst[8 + dstbase], _mm256_unpackhi_ps(r0, r1));
		}

		{
			const __m256 r0 = _mm256_unpackhi_ps(v_0, v_2);
			const __m256 r1 = _mm256_unpackhi_ps(v_1, v_3);

			_mm256_store_ps(&dst[16 + dstbase], _mm256_unpacklo_ps(r0, r1));
			_mm256_store_ps(&dst[24 + dstbase], _mm256_unpackhi_ps(r0, r1));
		}
	}

	for(int xi = 0; xi < _REM4_; ++xi)
	{
		const int dstbase = 8 * xi;
		LUNROLL(c, 0, 7, `dnl
		dst[eval(c + 8 * _NNICE4_) + dstbase] = src[eval(_NNICE4_ + _N_ * c) + xi];
		')
	}
}

void aos8_to_soa (
const float * __restrict__ const src, 
float * __restrict__ const dst)
{
	for(int base = 0; base < _NNICE_; base += 8)
	{
		const int srcbase = 8 * base;

		LUNROLL(pass, 0, 1, `
		{
		LUNROLL(c, 0, 3, `
		const __m256 TMP(q, c) = _mm256_castps128_ps256(_mm_load_ps(&src[srcbase + eval(4 * (pass + 2 * c))]));')
		LUNROLL(c, 0, 3, `
		const __m256 TMP(r, c) = _mm256_insertf128_ps(TMP(q, c), _mm_load_ps(&src[srcbase + eval(4 * (pass + 2 * (4 + c)))]), 1);')

		LUNROLL(t, 0, 1, `
		{
			const __m256 s_0 = ifelse(t,0,_mm256_unpacklo_ps,_mm256_unpackhi_ps)(TMP(r, 0), TMP(r, 2));
			const __m256 s_1 = ifelse(t,0,_mm256_unpacklo_ps,_mm256_unpackhi_ps)(TMP(r, 1), TMP(r, 3));

			_mm256_storeu_ps(&dst[base + _N_ * eval(0 +  2 * (t + 2 * pass))], _mm256_unpacklo_ps(s_0, s_1));
			_mm256_storeu_ps(&dst[base + _N_ * eval(1 +  2 * (t + 2 * pass))], _mm256_unpackhi_ps(s_0, s_1));
		}')
		}')
	}

	for(int xi = 0; xi < _REM_; ++xi)
	{
		const int srcbase = 8 * xi;

		LUNROLL(c, 0, 7, `dnl
		dst[eval(_NNICE_ + _N_ * c) + xi] = src[eval(c + 8 * _NNICE_) + srcbase];
		')
	}
}
') #end of AVX KERNELS

divert(0)
ifelse(_AVX_, 1, AVXKERNELS, CPPKERNELS)

