#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

uint64_t rdtsc()
{
    unsigned int lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

enum { N = _N_, ntimes = 100000 };

void aos8_from_soa (
    const float * __restrict__ const src,
    float * __restrict__ const dst);

void aos8_to_soa (
    const float * __restrict__ const src,
    float * __restrict__ const dst);

float data0[N * 8], data1[N * 8];

int main()
{
    int i, j;

    for (i = 0; i < 8; ++i)
	for (j = 0; j < N; ++j)
	    data0[j + N * i] = j + N * i;

    aos8_from_soa(data0, data1);

    for (j = 0; j < N; ++j)
	for (i = 0; i < 8; ++i)
	{
	    float expected = j + N * i;

	    if (data1[i + 8 * j] != expected)
	    {
		fprintf(stderr, "error: aos8_from_soa: consistency check failed.\n");

		return EXIT_FAILURE;
	    }
	}

    aos8_to_soa(data1, data0);

    for (j = 0; j < N; ++j)
	for (i = 0; i < 8; ++i)
	{
	    float expected = j + N * i;

	    if (data0[j + N * i] != expected)
	    {
		fprintf(stderr, "error: aos8_to_soa: consistency check failed.\n");

		return EXIT_FAILURE;
	    }
	}

    printf("consistency check passed\n");

    const uint64_t t0 = rdtsc();

    for(i = 0; i < ntimes; ++i)
	aos8_to_soa(data0, data1);

    const uint64_t t1 = rdtsc();

    for(i = 0; i < ntimes; ++i)
	aos8_from_soa(data0, data1);

    const uint64_t t2 = rdtsc();

    printf("THROUGHPUT:\n"
	   "TO-AOS   %.2f B/C\n"
	   "FROM-AOS %.2f B/C\n",
	   (double)ntimes * sizeof(float) * N * 8 / (double)(t1 - t0),
	   (double)ntimes * sizeof(float) * N * 8 / (double)(t2 - t1));

    return EXIT_SUCCESS;
}
