#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

uint64_t rdtsc()
{
    unsigned int lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

enum { N = _N_, N2 = N * N, N3 = N2 * N, ntimes = 10000 };

void transpose_xz(float * __restrict__ const data);

float data[N3];

int main()
{
    int i, j, k;

    for (i = 0; i < N; ++i)
	for(j = 0; j < N; ++j)
	    for(int k = 0; k < N; ++k)
		data[k + N * j + N2 * i] = k + N * j + N2 * i;

    transpose_xz(data);

    for (i = 0; i < N; ++i)
	for(j = 0; j < N; ++j)
	    for(k = 0; k < N; ++k)
	    {
		float expected = i + N * j + N2 * k;
		
		if (data[k + N * j + N2 * i] != expected)
		{
		    fprintf(stderr, "error: consistency check failed.\n");
		    
		    return EXIT_FAILURE;
		}
	    }

    printf("consistency check passed\n");

    const uint64_t t0 = rdtsc();

    for(i = 0; i < ntimes; ++i)
	transpose_xz(data);

    const uint64_t t1 = rdtsc();

    printf("THROUGHPUT: %.2f B/C\n",
	   (double)ntimes * sizeof(float) * N3 / (double)(t1 - t0));

    return EXIT_SUCCESS;
}
