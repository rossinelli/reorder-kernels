This subfolder generates and deploys object files with the following signature:

```void transpose_xz(float * const data);```

Remarks: 
- The function name changes according to the makefile variable "name".
- Size of the transposition must be provided at compile time.
- For sizes that are multiple of 8, ```data``` must be 32-byte aligned. 