divert(-1)
include(../share/util.m4)

define(_NNICE_, eval(_N_ & ~7))
define(_NM1_, eval(_N_ - 1))
divert(0)

ifelse(_AVX_,1, `include(../share/macros-avx-float.h)')

#ifndef _AVX_
#include "macros.h"
#endif

enum { N = _N_, NNICE = N & ~7, N2 = N * N };

void transpose_xz (float * __restrict__ const data)
{
    float __attribute__((aligned(64))) tmp[TILE2];

    for(int z = 0; z < NNICE; z += TILE)
    {
	float * __restrict__ const base0 = data + N2 * z;
	float * __restrict__ const base1 = data + z;

	for(int y = 0; y < N; ++y)
	{
	    float * __restrict__ const r0 = base0 + z + N * y;

	    LOAD_TRANSPOSED(r0, tmp, N2);

	    WRITE(tmp, r0, N2);
	}

	for(int x = z + TILE; x < NNICE; x += TILE)
	{
	    float * __restrict__ const r0 = base0 + x;
	    float * __restrict__ const r1 = base1 + N2 * x;

	    for(int y = 0; y < N; ++y)
	    {
		float * __restrict__ const t0 = r0 + N * y;
		float * __restrict__ const t1 = r1 + N * y;

		LOAD_TRANSPOSED(t0, tmp, N2);
		LTW(t1, t0, N2);
		WRITE(tmp, t1, N2);
	    }
	}
    }

    ifelse(eval(_NNICE_ <= _NM1_), 1, `
    /* handling arbitrary N */
    LUNROLL(z, _NNICE_, _NM1_, `
        for(int x = 0; x < z; ++x)
            for(int y = 0; y < N; ++y)
            {
                const float t0 = data[x + N * (y + N * z)];
                const float t1 = data[z + N * (y + N * x)];

                data[x + N * (y + N * z)] = t1;
                data[z + N * (y + N * x)] = t0;
            }')')
}
