enum { TILE = 8, TILE2 = TILE * TILE };

#include <immintrin.h>

#define LOAD_TRANSPOSED_HALF(in, out, STRIDE)							\
    do																	\
    {																	\
		const __m256 q_0 = _mm256_castps128_ps256						\
			(_mm_loadu_ps(in + STRIDE * 0));							\
		const __m256 q_1 = _mm256_castps128_ps256						\
			(_mm_loadu_ps(in + STRIDE * 1));							\
		const __m256 q_2 = _mm256_castps128_ps256						\
			(_mm_loadu_ps(in + STRIDE * 2));							\
		const __m256 q_3 = _mm256_castps128_ps256						\
			(_mm_loadu_ps(in + STRIDE * 3));							\
																		\
		const __m256 r_0 = _mm256_insertf128_ps							\
			(q_0, _mm_loadu_ps(in + STRIDE * 4), 1);					\
		const __m256 r_1 = _mm256_insertf128_ps							\
			(q_1, _mm_loadu_ps(in + STRIDE * 5), 1);					\
		const __m256 r_2 = _mm256_insertf128_ps							\
			(q_2, _mm_loadu_ps(in + STRIDE * 6), 1);					\
		const __m256 r_3 = _mm256_insertf128_ps							\
			(q_3, _mm_loadu_ps(in + STRIDE * 7), 1);					\
																		\
		{																\
			const __m256 s_0 = _mm256_unpacklo_ps(r_0, r_2);			\
			const __m256 s_1 = _mm256_unpacklo_ps(r_1, r_3);			\
			_mm256_store_ps(out + 8 * 0, _mm256_unpacklo_ps(s_0, s_1));	\
			_mm256_store_ps(out + 8 * 1, _mm256_unpackhi_ps(s_0, s_1));	\
		}																\
		{																\
			const __m256 s_0 = _mm256_unpackhi_ps(r_0, r_2);			\
			const __m256 s_1 = _mm256_unpackhi_ps(r_1, r_3);			\
			_mm256_store_ps(out + 8 * 2, _mm256_unpacklo_ps(s_0, s_1));	\
			_mm256_store_ps(out + 8 * 3, _mm256_unpackhi_ps(s_0, s_1));	\
		}																\
    }																	\
    while(0)

#define LOAD_TRANSPOSED(in, out, STRIDE)					\
	do														\
	{														\
	    LOAD_TRANSPOSED_HALF(in, out, STRIDE);				\
	    LOAD_TRANSPOSED_HALF(in + 4, out + 8 * 4, STRIDE);	\
	}														\
	while(0)

#define WRITE(in, out, STRIDE)							\
	do													\
	{													\
		_mm256_storeu_ps(out + STRIDE * 0,				\
						 _mm256_loadu_ps(in + 8 * 0));	\
		_mm256_storeu_ps(out + STRIDE * 1,				\
						 _mm256_loadu_ps(in + 8 * 1));	\
		_mm256_storeu_ps(out + STRIDE * 2,				\
						 _mm256_loadu_ps(in + 8 * 2));	\
		_mm256_storeu_ps(out + STRIDE * 3,				\
						 _mm256_loadu_ps(in + 8 * 3));	\
		_mm256_storeu_ps(out + STRIDE * 4,				\
						 _mm256_loadu_ps(in + 8 * 4));	\
		_mm256_storeu_ps(out + STRIDE * 5,				\
						 _mm256_loadu_ps(in + 8 * 5));	\
		_mm256_storeu_ps(out + STRIDE * 6,				\
						 _mm256_loadu_ps(in + 8 * 6));	\
		_mm256_storeu_ps(out + STRIDE * 7,				\
						 _mm256_load_ps(in + 8 * 7));	\
	}													\
	while(0)

#define LTW_HALF(in, out, STRIDE)										\
	do																	\
	{																	\
	    const __m256 q_0 = _mm256_castps128_ps256						\
			(_mm_loadu_ps(in + STRIDE * 0));							\
	    const __m256 q_1 = _mm256_castps128_ps256						\
			(_mm_loadu_ps(in + STRIDE * 1));							\
	    const __m256 q_2 = _mm256_castps128_ps256						\
			(_mm_loadu_ps(in + STRIDE * 2));							\
	    const __m256 q_3 = _mm256_castps128_ps256						\
			(_mm_loadu_ps(in + STRIDE * 3));							\
																		\
	    const __m256 r_0 = _mm256_insertf128_ps							\
			(q_0, _mm_loadu_ps(in + STRIDE * 4), 1);					\
	    const __m256 r_1 = _mm256_insertf128_ps							\
			(q_1, _mm_loadu_ps(in + STRIDE * 5), 1);					\
	    const __m256 r_2 = _mm256_insertf128_ps							\
			(q_2, _mm_loadu_ps(in + STRIDE * 6), 1);					\
	    const __m256 r_3 = _mm256_insertf128_ps							\
			(q_3, _mm_loadu_ps(in + STRIDE * 7), 1);					\
																		\
	    {																\
			const __m256 s_0 = _mm256_unpacklo_ps(r_0, r_2);			\
			const __m256 s_1 = _mm256_unpacklo_ps(r_1, r_3);			\
			_mm256_storeu_ps(out + STRIDE * 0, _mm256_unpacklo_ps(s_0, s_1)); \
			_mm256_storeu_ps(out + STRIDE * 1, _mm256_unpackhi_ps(s_0, s_1)); \
	    }																\
	    {																\
			const __m256 s_0 = _mm256_unpackhi_ps(r_0, r_2);			\
			const __m256 s_1 = _mm256_unpackhi_ps(r_1, r_3);			\
			_mm256_storeu_ps(out + STRIDE * 2, _mm256_unpacklo_ps(s_0, s_1)); \
			_mm256_storeu_ps(out + STRIDE * 3, _mm256_unpackhi_ps(s_0, s_1)); \
	    }																\
	}																	\
	while(0)

#define LTW(in, out, STRIDE)						\
	do												\
	{												\
		LTW_HALF(in, out, STRIDE);					\
		LTW_HALF(in + 4, out + 4 * STRIDE, STRIDE);	\
	}												\
	while(0)
