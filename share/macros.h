#include <stddef.h>

enum { TILE = 4, TILE2 = TILE * TILE };

#define LOAD_TRANSPOSED(in, out, STRIDE)					\
	do														\
	{														\
		real __attribute__((aligned(64))) staging[TILE2];	\
															\
		for(int j = 0; j < TILE; ++j)						\
		{													\
			const real * __restrict__ const s =			\
				in + STRIDE * j;							\
															\
			real * __restrict__ const d =					\
				staging + TILE * j;							\
															\
			for(int i = 0; i < TILE; ++i)					\
				d[i] = s[i];								\
		}													\
															\
		for(int j = 0; j < TILE; ++j)						\
			for(int i = 0; i < TILE; ++i)					\
				out[i + TILE * j] = staging[j + TILE * i];	\
															\
	}														\
	while(0)

#define WRITE(in, out, STRIDE)									\
	do															\
	{															\
		for(int j = 0; j < TILE; ++j)							\
		{														\
			const real * __restrict__ const s = in + TILE * j;	\
			real * __restrict__ const d = out + STRIDE * j;	\
																\
			for(int i = 0; i < TILE; ++i)						\
				d[i] = s[i];									\
		}														\
	}															\
	while(0)

#define LTW(in, out, STRIDE)							\
	for (ptrdiff_t y = 0; y < TILE; ++y)				\
		for(ptrdiff_t x = 0; x < TILE; ++x)				\
			out[y + x * STRIDE] = in[x + STRIDE * y];
