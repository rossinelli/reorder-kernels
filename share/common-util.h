#pragma once
#ifdef __cplusplus
#include <cstdlib>
#include <cstdio>
#else
#include <stdlib.h>
#include <stdio.h>
#endif

#include <posix-util.h>

#define CHECK(stmt, ...)			\
    do						\
    {						\
	if (!(stmt))				\
	{					\
	    fprintf(stderr,			\
		    __VA_ARGS__);		\
						\
	    exit(EXIT_FAILURE);			\
	}					\
    }						\
    while(0)

#define LOG_TXT(pathname, ...)			\
    do						\
    {						\
	FILE * f;				\
	POSIX_CHECK(f = fopen(pathname, "w"));	\
	fprintf(f, __VA_ARGS__);		\
	POSIX_CHECK(0 == fclose(f));		\
    }						\
    while(0)

#define PARSE_TXT(pathname, ...)		\
    do						\
    {						\
	FILE * f;				\
	POSIX_CHECK(f = fopen(pathname, "r"));	\
	fscanf(f, __VA_ARGS__);			\
	POSIX_CHECK(0 == fclose(f));		\
    }						\
    while(0)

#define READENV(x, op)				\
    do						\
    {						\
	if (getenv(#x))				\
	    x = op(getenv(#x));			\
    }						\
    while(0)

#include <string.h>

#define READENVV(x, fmt, sep)						\
  do									\
    {									\
      char * str = getenv(#x);						\
      if (str)								\
	{								\
	  char * ptr = strtok(str, sep);				\
	  const ptrdiff_t cnt = sizeof(x) / sizeof(x[0]);		\
									\
	  int i = 0;							\
	  while (ptr)							\
	    {								\
	      if (i >= cnt && ptr)					\
		{							\
		  fprintf(stderr,					\
			  "warning: READENVV couldn't properly read <%s> remaining <%s>\n", \
			  #x, ptr);					\
									\
		  break;						\
		}							\
									\
	      sscanf(ptr, fmt, &x[i++]);				\
									\
	      ptr = strtok(NULL, sep);					\
	    }								\
	}								\
    }									\
  while(0)								\

#define PRINTENV(f, x, p)			\
    fprintf(f, #x ": " p "\n", x)

#ifndef MAX
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#endif
#ifndef MIN
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#endif

#include <stdint.h>

static inline uint64_t rdtsc(void)
{
#ifdef __APPLE__
    return __builtin_readcyclecounter();
#else
    unsigned int lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
#endif
}

#include <stddef.h>

static inline int valid_roi (
    const ptrdiff_t xsize,
    const ptrdiff_t ysize,
    const ptrdiff_t zsize,
    const ptrdiff_t xstart,
    const ptrdiff_t ystart,
    const ptrdiff_t zstart,
    const ptrdiff_t xend,
    const ptrdiff_t yend,
    const ptrdiff_t zend)
{
    return
	xsize > 0 &&
	ysize > 0 &&
	zsize > 0 &&
	xend > 0 &&
	yend > 0 &&
	zend > 0 &&
	xstart >= 0 &&
	ystart >= 0 &&
	zstart >= 0 &&
	xstart < xend &&
	ystart < yend &&
	zstart < zend &&
	xend <= xsize &&
	yend <= ysize &&
	zend <= zsize ;
}

typedef struct { ptrdiff_t start, count; } workload;
typedef workload workload_t;

inline static workload divide_workload (
    const ptrdiff_t w,
    const ptrdiff_t wn,
    const ptrdiff_t tn)
{
    const ptrdiff_t share = tn / wn;
    const ptrdiff_t rmnd = tn % wn;

    const ptrdiff_t s = share * w + MIN(w, rmnd);
    const ptrdiff_t c = share + (w < rmnd);

    workload retval = { s, c };

    return retval;
}

typedef struct { FILE * f; void * ptr; size_t sz; } dybuf_t;

static void dybuf_init (dybuf_t * const d)
{
#ifdef _WIN32
        d->sz = 0;
        d->ptr = NULL;
        d->f = NULL;
#else
	POSIX_CHECK(d->f = open_memstream((char **)&d->ptr, &d->sz));
#endif
}

static void dybuf_append (
        dybuf_t * const d,
        const void * const ptr,
        const size_t sz )
{
#ifdef _WIN32
	if ((size_t)d->f < d->sz + sz)
	{
		d->f = 2 * d->sz + sz;
		POSIX_CHECK(d->ptr = realloc(d->ptr, (size_t)d->f));
	}

	memcpy(d->sz + d->ptr, ptr, sz);
	d->sz += sz;
#else
	POSIX_CHECK(fwrite(ptr, sz, 1, d->f) == 1);
#endif
}

static void dybuf_from_file (
	dybuf_t * d,
	FILE * f)
{
	while (1)
	{
		enum { BUFSZ = 1 << 20 };

		char buf[BUFSZ];

		/* tuple count */
		const size_t n = fread(buf, 1, BUFSZ, f);

		dybuf_append(d, buf, n);

		if (BUFSZ != n)
			break;
	}
}

static size_t dybuf_freeze (
	dybuf_t * const d,
	const size_t esz )
{
#ifndef _WIN32
	POSIX_CHECK(0 == fclose(d->f));
#endif
	CHECK(0 == d->sz % esz,
		  "error: dybuf has unexpected length\n");
	return d->sz / esz;
}

#include <assert.h>
/* erase and recompact */
static void ear (
		/* input size */
		const ptrdiff_t isz,
		/* element size */
		const ptrdiff_t esz,
		void * const inout,
		/* amount of entries to erase */
		const ptrdiff_t ec,
		/* sorted entries to erase */
		const ptrdiff_t * const es
	 )
{
	/* input count */
	const ptrdiff_t ic = isz / esz;

	ptrdiff_t src = 0, dst = 0;

	for (ptrdiff_t e = 0; e <= ec; ++e)
	{
		ptrdiff_t len;

		if (e)
			if (e < ec)
				len = es[e] - (es[e - 1] + 1);
			else
				len = ic - (es[e - 1] + 1);
		else
			len = es[e];

		len *= esz;

		assert(0 <= len);

		if (len)
		{
			memmove(dst + (char *)inout, src + (char *)inout, len);

			/* src never sits on dropped entries */
			for (ptrdiff_t j = 0; j < ec; ++j)
				assert(src != es[j] * esz);
		}

		dst += len;
		src += len + (e < ec) * esz;
	}

	assert((ic - ec) * esz == dst);
	assert(ic * esz == src);
}

#include <sys/stat.h>
inline static void mkdir_p (const char *dir)
{
	char tmp[256];
	char *p = NULL;

	snprintf(tmp, sizeof(tmp),"%s",dir);

	size_t len = strlen(tmp);

	if(tmp[len - 1] == '/')
		tmp[len - 1] = 0;

	for(p = tmp + 1; *p; p++)
		if(*p == '/')
		{
			*p = 0;
			mkdir(tmp, S_IRWXU);
			*p = '/';
		}

	mkdir(tmp, S_IRWXU);
}

#ifdef _WIN32
/* stream mode */
#include <fcntl.h>
#include <io.h>
#endif
