enum { TILE = 4, TILE2 = TILE * TILE };
#include <immintrin.h>
#include <stddef.h>

#define LOAD_TRANSPOSED_HALF(in, out, STRIDE)							\
	do																	\
	{																	\
		const __m256d q_0 = _mm256_castpd128_pd256						\
			(_mm_loadu_pd(in + STRIDE * 0));							\
		const __m256d q_1 = _mm256_castpd128_pd256						\
			(_mm_loadu_pd(in + STRIDE * 1));							\
																		\
		const __m256d r_0 = _mm256_insertf128_pd						\
			(q_0, _mm_loadu_pd(in + STRIDE * 2), 1);					\
		const __m256d r_1 = _mm256_insertf128_pd						\
			(q_1, _mm_loadu_pd(in + STRIDE * 3), 1);					\
																		\
		{																\
			_mm256_store_pd(out + 4 * 0, _mm256_unpacklo_pd(r_0, r_1));	\
			_mm256_store_pd(out + 4 * 1, _mm256_unpackhi_pd(r_0, r_1));	\
		}																\
	}																	\
	while(0)

#define LOAD_TRANSPOSED(in, out, STRIDE)					\
	do														\
	{														\
		LOAD_TRANSPOSED_HALF(in, out, STRIDE);				\
		LOAD_TRANSPOSED_HALF(in + 2, out + 4 * 2, STRIDE);	\
	}														\
	while(0)

#define WRITE(in, out, STRIDE)							\
	do													\
	{													\
		_mm256_storeu_pd(out + STRIDE * 0,				\
						 _mm256_load_pd(in + 4 * 0));	\
		_mm256_storeu_pd(out + STRIDE * 1,				\
						 _mm256_load_pd(in + 4 * 1));	\
		_mm256_storeu_pd(out + STRIDE * 2,				\
						 _mm256_load_pd(in + 4 * 2));	\
		_mm256_storeu_pd(out + STRIDE * 3,				\
						 _mm256_load_pd(in + 4 * 3));	\
	}													\
	while(0)

#define LTW_HALF(in, out, STRIDE)										\
	do																	\
	{																	\
		const __m256d q_0 = _mm256_castpd128_pd256						\
			(_mm_loadu_pd(in + STRIDE * 0));							\
		const __m256d q_1 = _mm256_castpd128_pd256						\
			(_mm_loadu_pd(in + STRIDE * 1));							\
																		\
		const __m256d r_0 = _mm256_insertf128_pd						\
			(q_0, _mm_loadu_pd(in + STRIDE * 2), 1);					\
		const __m256d r_1 = _mm256_insertf128_pd						\
			(q_1, _mm_loadu_pd(in + STRIDE * 3), 1);					\
																		\
		{																\
			_mm256_storeu_pd(out + STRIDE * 0, _mm256_unpacklo_pd(r_0, r_1)); \
			_mm256_storeu_pd(out + STRIDE * 1, _mm256_unpackhi_pd(r_0, r_1)); \
		}																\
	}																	\
	while(0)

#define LTW(in, out, STRIDE)						\
	do												\
	{												\
		LTW_HALF(in, out, STRIDE);					\
		LTW_HALF(in + 2, out + 2 * STRIDE, STRIDE);	\
	}												\
	while(0)
