#include <stdint.h>

enum
{ TILE = 16, TILE2 = TILE * TILE };

#include <immintrin.h>

/* _IMPL_ :
   0: default
   1: 256bit building blocks
   2: zboson: https://stackoverflow.com/questions/29519222/how-to-transpose-a-16x16-matrix-using-simd-instructions
   3: zboson2: second SO answer
*/
#define _IMPL_ 3

inline static void
LOAD_TRANSPOSED (const float *const __restrict__ in, float *const __restrict__ out, const int stride)
{
#if 0 == _IMPL_
	const int stride4 = stride * 4;

	{
		const float *const __restrict__ data0 = in;

		__m512 a0;

		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data0), 0);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data0 + stride4 * 1), 1);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data0 + stride4 * 2), 2);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data0 + stride4 * 3), 3);
		__m512 a1;

		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data0 + stride * 1), 0);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data0 + stride * 1 + stride4 * 1), 1);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data0 + stride * 1 + stride4 * 2), 2);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data0 + stride * 1 + stride4 * 3), 3);
		__m512 a2;

		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data0 + stride * 2), 0);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data0 + stride * 2 + stride4 * 1), 1);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data0 + stride * 2 + stride4 * 2), 2);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data0 + stride * 2 + stride4 * 3), 3);
		__m512 a3;

		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data0 + stride * 3), 0);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data0 + stride * 3 + stride4 * 1), 1);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data0 + stride * 3 + stride4 * 2), 2);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data0 + stride * 3 + stride4 * 3), 3);

		const __m512 b0 = _mm512_unpacklo_ps (a0, a2);
		const __m512 b1 = _mm512_unpackhi_ps (a0, a2);
		const __m512 b2 = _mm512_unpacklo_ps (a1, a3);
		const __m512 b3 = _mm512_unpackhi_ps (a1, a3);

		const __m512 c0 = _mm512_unpacklo_ps (b0, b2);
		const __m512 c1 = _mm512_unpackhi_ps (b0, b2);
		const __m512 c2 = _mm512_unpacklo_ps (b1, b3);
		const __m512 c3 = _mm512_unpackhi_ps (b1, b3);


		_mm512_store_ps (out + TILE * 0, c0);
		_mm512_store_ps (out + TILE * 1, c1);
		_mm512_store_ps (out + TILE * 2, c2);
		_mm512_store_ps (out + TILE * 3, c3);
	}
	{
		const float *const __restrict__ data1 = in + 4 * 1;

		__m512 a0;

		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data1), 0);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data1 + stride4 * 1), 1);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data1 + stride4 * 2), 2);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data1 + stride4 * 3), 3);
		__m512 a1;

		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data1 + stride * 1), 0);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data1 + stride * 1 + stride4 * 1), 1);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data1 + stride * 1 + stride4 * 2), 2);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data1 + stride * 1 + stride4 * 3), 3);
		__m512 a2;

		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data1 + stride * 2), 0);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data1 + stride * 2 + stride4 * 1), 1);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data1 + stride * 2 + stride4 * 2), 2);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data1 + stride * 2 + stride4 * 3), 3);
		__m512 a3;

		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data1 + stride * 3), 0);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data1 + stride * 3 + stride4 * 1), 1);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data1 + stride * 3 + stride4 * 2), 2);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data1 + stride * 3 + stride4 * 3), 3);

		const __m512 b0 = _mm512_unpacklo_ps (a0, a2);
		const __m512 b1 = _mm512_unpackhi_ps (a0, a2);
		const __m512 b2 = _mm512_unpacklo_ps (a1, a3);
		const __m512 b3 = _mm512_unpackhi_ps (a1, a3);

		const __m512 c0 = _mm512_unpacklo_ps (b0, b2);
		const __m512 c1 = _mm512_unpackhi_ps (b0, b2);
		const __m512 c2 = _mm512_unpacklo_ps (b1, b3);
		const __m512 c3 = _mm512_unpackhi_ps (b1, b3);


		_mm512_store_ps (out + TILE * 4, c0);
		_mm512_store_ps (out + TILE * 5, c1);
		_mm512_store_ps (out + TILE * 6, c2);
		_mm512_store_ps (out + TILE * 7, c3);
	}
	{
		const float *const __restrict__ data2 = in + 4 * 2;

		__m512 a0;

		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data2), 0);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data2 + stride4 * 1), 1);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data2 + stride4 * 2), 2);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data2 + stride4 * 3), 3);
		__m512 a1;

		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data2 + stride * 1), 0);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data2 + stride * 1 + stride4 * 1), 1);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data2 + stride * 1 + stride4 * 2), 2);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data2 + stride * 1 + stride4 * 3), 3);
		__m512 a2;

		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data2 + stride * 2), 0);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data2 + stride * 2 + stride4 * 1), 1);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data2 + stride * 2 + stride4 * 2), 2);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data2 + stride * 2 + stride4 * 3), 3);
		__m512 a3;

		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data2 + stride * 3), 0);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data2 + stride * 3 + stride4 * 1), 1);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data2 + stride * 3 + stride4 * 2), 2);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data2 + stride * 3 + stride4 * 3), 3);

		const __m512 b0 = _mm512_unpacklo_ps (a0, a2);
		const __m512 b1 = _mm512_unpackhi_ps (a0, a2);
		const __m512 b2 = _mm512_unpacklo_ps (a1, a3);
		const __m512 b3 = _mm512_unpackhi_ps (a1, a3);

		const __m512 c0 = _mm512_unpacklo_ps (b0, b2);
		const __m512 c1 = _mm512_unpackhi_ps (b0, b2);
		const __m512 c2 = _mm512_unpacklo_ps (b1, b3);
		const __m512 c3 = _mm512_unpackhi_ps (b1, b3);


		_mm512_store_ps (out + TILE * 8, c0);
		_mm512_store_ps (out + TILE * 9, c1);
		_mm512_store_ps (out + TILE * 10, c2);
		_mm512_store_ps (out + TILE * 11, c3);
	}
	{
		const float *const __restrict__ data3 = in + 4 * 3;

		__m512 a0;

		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data3), 0);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data3 + stride4 * 1), 1);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data3 + stride4 * 2), 2);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data3 + stride4 * 3), 3);
		__m512 a1;

		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data3 + stride * 1), 0);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data3 + stride * 1 + stride4 * 1), 1);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data3 + stride * 1 + stride4 * 2), 2);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data3 + stride * 1 + stride4 * 3), 3);
		__m512 a2;

		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data3 + stride * 2), 0);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data3 + stride * 2 + stride4 * 1), 1);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data3 + stride * 2 + stride4 * 2), 2);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data3 + stride * 2 + stride4 * 3), 3);
		__m512 a3;

		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data3 + stride * 3), 0);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data3 + stride * 3 + stride4 * 1), 1);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data3 + stride * 3 + stride4 * 2), 2);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data3 + stride * 3 + stride4 * 3), 3);

		const __m512 b0 = _mm512_unpacklo_ps (a0, a2);
		const __m512 b1 = _mm512_unpackhi_ps (a0, a2);
		const __m512 b2 = _mm512_unpacklo_ps (a1, a3);
		const __m512 b3 = _mm512_unpackhi_ps (a1, a3);

		const __m512 c0 = _mm512_unpacklo_ps (b0, b2);
		const __m512 c1 = _mm512_unpackhi_ps (b0, b2);
		const __m512 c2 = _mm512_unpacklo_ps (b1, b3);
		const __m512 c3 = _mm512_unpackhi_ps (b1, b3);


		_mm512_store_ps (out + TILE * 12, c0);
		_mm512_store_ps (out + TILE * 13, c1);
		_mm512_store_ps (out + TILE * 14, c2);
		_mm512_store_ps (out + TILE * 15, c3);
	}
#elif 1 == _IMPL_
	__m512i a0;
	a0 = _mm512_inserti64x4 (a0, _mm256_loadu_si256((__m256i *)(in + stride * 0)), 0);
	a0 = _mm512_inserti64x4 (a0, _mm256_loadu_si256((__m256i *)(in + stride * 8)), 1);

	__m512i a1;
	a1 = _mm512_inserti64x4 (a1, _mm256_loadu_si256((__m256i *)(in + stride * 1)), 0);
	a1 = _mm512_inserti64x4 (a1, _mm256_loadu_si256((__m256i *)(in + stride * 9)), 1);

	__m512i a2;
	a2 = _mm512_inserti64x4 (a2, _mm256_loadu_si256((__m256i *)(in + stride * 2)), 0);
	a2 = _mm512_inserti64x4 (a2, _mm256_loadu_si256((__m256i *)(in + stride * 10)), 1);

	__m512i a3;
	a3 = _mm512_inserti64x4 (a3, _mm256_loadu_si256((__m256i *)(in + stride * 3)), 0);
	a3 = _mm512_inserti64x4 (a3, _mm256_loadu_si256((__m256i *)(in + stride * 11)), 1);

	__m512i p0, p1, p2, p3;

	{
		__m512i r0 = _mm512_unpacklo_epi32(a0, a2);
		__m512i r1 = _mm512_unpacklo_epi32(a1, a3);

		p0 = _mm512_unpacklo_epi32(r0, r1);
		p1 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i r0 = _mm512_unpackhi_epi32(a0, a2);
		__m512i r1 = _mm512_unpackhi_epi32(a1, a3);

		p2 = _mm512_unpacklo_epi32(r0, r1);
		p3 = _mm512_unpackhi_epi32(r0, r1);
	}

	__m512i b0;
	b0 = _mm512_inserti64x4 (b0, _mm256_loadu_si256((__m256i *)(in + stride * 4)), 0);
	b0 = _mm512_inserti64x4 (b0, _mm256_loadu_si256((__m256i *)(in + stride * 12)), 1);

	__m512i b1;
	b1 = _mm512_inserti64x4 (b1, _mm256_loadu_si256((__m256i *)(in + stride * 5)), 0);
	b1 = _mm512_inserti64x4 (b1, _mm256_loadu_si256((__m256i *)(in + stride * 13)), 1);

	__m512i b2;
	b2 = _mm512_inserti64x4 (b2, _mm256_loadu_si256((__m256i *)(in + stride * 6)), 0);
	b2 = _mm512_inserti64x4 (b2, _mm256_loadu_si256((__m256i *)(in + stride * 14)), 1);

	__m512i b3;
	b3 = _mm512_inserti64x4 (b3, _mm256_loadu_si256((__m256i *)(in + stride * 7)), 0);
	b3 = _mm512_inserti64x4 (b3, _mm256_loadu_si256((__m256i *)(in + stride * 15)), 1);

	__m512i q0, q1, q2, q3;
	{
		__m512i r0 = _mm512_unpacklo_epi32(b0, b2);
		__m512i r1 = _mm512_unpacklo_epi32(b1, b3);

		q0 = _mm512_unpacklo_epi32(r0, r1);
		q1 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i r0 = _mm512_unpackhi_epi32(b0, b2);
		__m512i r1 = _mm512_unpackhi_epi32(b1, b3);

		q2 = _mm512_unpacklo_epi32(r0, r1);
		q3 = _mm512_unpackhi_epi32(r0, r1);
	}

	/* swap the 4x4 tiles */
	a0 = _mm512_inserti64x4 (a0, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 0)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p0, q0, 0x88);
		_mm512_storeu_si512(out, _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	a1 = _mm512_inserti64x4 (a1, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 1)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p1, q1, 0x88);
		_mm512_storeu_si512(out + 16 * 1,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	a2 = _mm512_inserti64x4 (a2, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 2)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p2, q2, 0x88);
		_mm512_storeu_si512(out + 16 * 2,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	a3 = _mm512_inserti64x4 (a3, _mm256_loadu_si256((__m256i *)(in + 8 +  stride * 3)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p3, q3, 0x88);
		_mm512_storeu_si512(out + 16 * 3,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	b0 = _mm512_inserti64x4 (b0, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 4)), 0);
	{
		__m512i t0 = _mm512_shuffle_i32x4(p0, q0, 0xdd);
		_mm512_storeu_si512(out + 16 * 4,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	b1 = _mm512_inserti64x4 (b1, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 5)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p1, q1, 0xdd);
		_mm512_storeu_si512(out + 16 * 5,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	b2 = _mm512_inserti64x4 (b2, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 6)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p2, q2, 0xdd);
		_mm512_storeu_si512(out + 16 * 6,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	b3 = _mm512_inserti64x4 (b3, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 7)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p3, q3, 0xdd);
		_mm512_storeu_si512(out + 16 * 7,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	a0 = _mm512_inserti64x4 (a0, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 8)), 1);
	a1 = _mm512_inserti64x4 (a1, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 9)), 1);
	a2 = _mm512_inserti64x4 (a2, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 10)), 1);
	a3 = _mm512_inserti64x4 (a3, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 11)), 1);

	b0 = _mm512_inserti64x4 (b0, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 12)), 1);
	b1 = _mm512_inserti64x4 (b1, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 13)), 1);
	b2 = _mm512_inserti64x4 (b2, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 14)), 1);
	b3 = _mm512_inserti64x4 (b3, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 15)), 1);

	{
		__m512i r0 = _mm512_unpacklo_epi32(a0, a2);
		__m512i r1 = _mm512_unpacklo_epi32(a1, a3);

		p0 = _mm512_unpacklo_epi32(r0, r1);
		p1 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i r0 = _mm512_unpackhi_epi32(a0, a2);
		__m512i r1 = _mm512_unpackhi_epi32(a1, a3);

		p2 = _mm512_unpacklo_epi32(r0, r1);
		p3 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i r0 = _mm512_unpacklo_epi32(b0, b2);
		__m512i r1 = _mm512_unpacklo_epi32(b1, b3);

		q0 = _mm512_unpacklo_epi32(r0, r1);
		q1 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i r0 = _mm512_unpackhi_epi32(b0, b2);
		__m512i r1 = _mm512_unpackhi_epi32(b1, b3);

		q2 = _mm512_unpacklo_epi32(r0, r1);
		q3 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p0, q0, 0x88);
		_mm512_storeu_si512(out + 16 * 8, _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p1, q1, 0x88);
		_mm512_storeu_si512(out + 16 * 9,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p2, q2, 0x88);
		_mm512_storeu_si512(out + 16 * 10,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p3, q3, 0x88);
		_mm512_storeu_si512(out + 16 * 11,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p0, q0, 0xdd);
		_mm512_storeu_si512(out + 16 * 12,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p1, q1, 0xdd);
		_mm512_storeu_si512(out + 16 * 13,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p2, q2, 0xdd);
		_mm512_storeu_si512(out + 16 * 14,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p3, q3, 0xdd);
		_mm512_storeu_si512(out + 16 * 15,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}
#elif 2 == _IMPL_

	__m512i t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, ta, tb, tc, td, te, tf;
	__m512i r0 = _mm512_loadu_si512(in + stride * 0);
	__m512i r1 = _mm512_loadu_si512(in + stride * 1);
	__m512i r2 = _mm512_loadu_si512(in + stride * 2);
	__m512i r3 = _mm512_loadu_si512(in + stride * 3);
	__m512i r4 = _mm512_loadu_si512(in + stride * 4);
	__m512i r5 = _mm512_loadu_si512(in + stride * 5);
	__m512i r6 = _mm512_loadu_si512(in + stride * 6);
	__m512i r7 = _mm512_loadu_si512(in + stride * 7);
	__m512i r8 = _mm512_loadu_si512(in + stride * 8);
	__m512i r9 = _mm512_loadu_si512(in + stride * 9);
	__m512i ra = _mm512_loadu_si512(in + stride * 10);
	__m512i rb = _mm512_loadu_si512(in + stride * 11);
	__m512i rc = _mm512_loadu_si512(in + stride * 12);
	__m512i rd = _mm512_loadu_si512(in + stride * 13);
	__m512i re = _mm512_loadu_si512(in + stride * 14);
	__m512i rf = _mm512_loadu_si512(in + stride * 15);

	t0 = _mm512_unpacklo_epi32(r0,r1); //   0  16   1  17   4  20   5  21   8  24   9  25  12  28  13  29
	t1 = _mm512_unpackhi_epi32(r0,r1); //   2  18   3  19   6  22   7  23  10  26  11  27  14  30  15  31
	t2 = _mm512_unpacklo_epi32(r2,r3); //  32  48  33  49 ...
	t3 = _mm512_unpackhi_epi32(r2,r3); //  34  50  35  51 ...
	t4 = _mm512_unpacklo_epi32(r4,r5); //  64  80  65  81 ...
	t5 = _mm512_unpackhi_epi32(r4,r5); //  66  82  67  83 ...
	t6 = _mm512_unpacklo_epi32(r6,r7); //  96 112  97 113 ...
	t7 = _mm512_unpackhi_epi32(r6,r7); //  98 114  99 115 ...
	t8 = _mm512_unpacklo_epi32(r8,r9); // 128 ...
	t9 = _mm512_unpackhi_epi32(r8,r9); // 130 ...
	ta = _mm512_unpacklo_epi32(ra,rb); // 160 ...
	tb = _mm512_unpackhi_epi32(ra,rb); // 162 ...
	tc = _mm512_unpacklo_epi32(rc,rd); // 196 ...
	td = _mm512_unpackhi_epi32(rc,rd); // 198 ...
	te = _mm512_unpacklo_epi32(re,rf); // 228 ...
	tf = _mm512_unpackhi_epi32(re,rf); // 230 ...

	r0 = _mm512_unpacklo_epi64(t0,t2); //   0  16  32  48 ...
	r1 = _mm512_unpackhi_epi64(t0,t2); //   1  17  33  49 ...
	r2 = _mm512_unpacklo_epi64(t1,t3); //   2  18  34  49 ...
	r3 = _mm512_unpackhi_epi64(t1,t3); //   3  19  35  51 ...
	r4 = _mm512_unpacklo_epi64(t4,t6); //  64  80  96 112 ...
	r5 = _mm512_unpackhi_epi64(t4,t6); //  65  81  97 114 ...
	r6 = _mm512_unpacklo_epi64(t5,t7); //  66  82  98 113 ...
	r7 = _mm512_unpackhi_epi64(t5,t7); //  67  83  99 115 ...
	r8 = _mm512_unpacklo_epi64(t8,ta); // 128 144 160 176 ...
	r9 = _mm512_unpackhi_epi64(t8,ta); // 129 145 161 178 ...
	ra = _mm512_unpacklo_epi64(t9,tb); // 130 146 162 177 ...
	rb = _mm512_unpackhi_epi64(t9,tb); // 131 147 163 179 ...
	rc = _mm512_unpacklo_epi64(tc,te); // 192 208 228 240 ...
	rd = _mm512_unpackhi_epi64(tc,te); // 193 209 229 241 ...
	re = _mm512_unpacklo_epi64(td,tf); // 194 210 230 242 ...
	rf = _mm512_unpackhi_epi64(td,tf); // 195 211 231 243 ...

	t0 = _mm512_shuffle_i32x4(r0, r4, 0x88); //   0  16  32  48   8  24  40  56  64  80  96  112 ...
	t1 = _mm512_shuffle_i32x4(r1, r5, 0x88); //   1  17  33  49 ...
	t2 = _mm512_shuffle_i32x4(r2, r6, 0x88); //   2  18  34  50 ...
	t3 = _mm512_shuffle_i32x4(r3, r7, 0x88); //   3  19  35  51 ...
	t4 = _mm512_shuffle_i32x4(r0, r4, 0xdd); //   4  20  36  52 ...
	t5 = _mm512_shuffle_i32x4(r1, r5, 0xdd); //   5  21  37  53 ...
	t6 = _mm512_shuffle_i32x4(r2, r6, 0xdd); //   6  22  38  54 ...
	t7 = _mm512_shuffle_i32x4(r3, r7, 0xdd); //   7  23  39  55 ...
	t8 = _mm512_shuffle_i32x4(r8, rc, 0x88); // 128 144 160 176 ...
	t9 = _mm512_shuffle_i32x4(r9, rd, 0x88); // 129 145 161 177 ...
	ta = _mm512_shuffle_i32x4(ra, re, 0x88); // 130 146 162 178 ...
	tb = _mm512_shuffle_i32x4(rb, rf, 0x88); // 131 147 163 179 ...
	tc = _mm512_shuffle_i32x4(r8, rc, 0xdd); // 132 148 164 180 ...
	td = _mm512_shuffle_i32x4(r9, rd, 0xdd); // 133 149 165 181 ...
	te = _mm512_shuffle_i32x4(ra, re, 0xdd); // 134 150 166 182 ...
	tf = _mm512_shuffle_i32x4(rb, rf, 0xdd); // 135 151 167 183 ...

	r0 = _mm512_shuffle_i32x4(t0, t8, 0x88); //   0  16  32  48  64  80  96 112 ... 240
	r1 = _mm512_shuffle_i32x4(t1, t9, 0x88); //   1  17  33  49  66  81  97 113 ... 241
	r2 = _mm512_shuffle_i32x4(t2, ta, 0x88); //   2  18  34  50  67  82  98 114 ... 242
	r3 = _mm512_shuffle_i32x4(t3, tb, 0x88); //   3  19  35  51  68  83  99 115 ... 243
	r4 = _mm512_shuffle_i32x4(t4, tc, 0x88); //   4 ...
	r5 = _mm512_shuffle_i32x4(t5, td, 0x88); //   5 ...
	r6 = _mm512_shuffle_i32x4(t6, te, 0x88); //   6 ...
	r7 = _mm512_shuffle_i32x4(t7, tf, 0x88); //   7 ...
	r8 = _mm512_shuffle_i32x4(t0, t8, 0xdd); //   8 ...
	r9 = _mm512_shuffle_i32x4(t1, t9, 0xdd); //   9 ...
	ra = _mm512_shuffle_i32x4(t2, ta, 0xdd); //  10 ...
	rb = _mm512_shuffle_i32x4(t3, tb, 0xdd); //  11 ...
	rc = _mm512_shuffle_i32x4(t4, tc, 0xdd); //  12 ...
	rd = _mm512_shuffle_i32x4(t5, td, 0xdd); //  13 ...
	re = _mm512_shuffle_i32x4(t6, te, 0xdd); //  14 ...
	rf = _mm512_shuffle_i32x4(t7, tf, 0xdd); //  15  31  47  63  79  96 111 127 ... 255

	_mm512_storeu_si512(out + 16 * 0, r0);
	_mm512_storeu_si512(out + 16 * 1, r1);
	_mm512_storeu_si512(out + 16 * 2, r2);
	_mm512_storeu_si512(out + 16 * 3, r3);
	_mm512_storeu_si512(out + 16 * 4, r4);
	_mm512_storeu_si512(out + 16 * 5, r5);
	_mm512_storeu_si512(out + 16 * 6, r6);
	_mm512_storeu_si512(out + 16 * 7, r7);
	_mm512_storeu_si512(out + 16 * 8, r8);
	_mm512_storeu_si512(out + 16 * 9, r9);
	_mm512_storeu_si512(out + 16 * 10, ra);
	_mm512_storeu_si512(out + 16 * 11, rb);
	_mm512_storeu_si512(out + 16 * 12, rc);
	_mm512_storeu_si512(out + 16 * 13, rd);
	_mm512_storeu_si512(out + 16 * 14, re);
	_mm512_storeu_si512(out + 16 * 15, rf);
#else
	__m512i t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, ta, tb, tc, td, te, tf;
	__m512i r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, ra, rb, rc, rd, re, rf;

	int mask;
	int64_t idx1[8] __attribute__((aligned(64))) = {2, 3, 0, 1, 6, 7, 4, 5};
	int64_t idx2[8] __attribute__((aligned(64))) = {1, 0, 3, 2, 5, 4, 7, 6};
	int32_t idx3[16] __attribute__((aligned(64))) = {1, 0, 3, 2, 5 ,4 ,7 ,6 ,9 ,8 , 11, 10, 13, 12 ,15, 14};
	__m512i vidx1 = _mm512_load_epi64(idx1);
	__m512i vidx2 = _mm512_load_epi64(idx2);
	__m512i vidx3 = _mm512_load_epi32(idx3);

	t0 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 0*stride+0])), _mm256_load_si256((__m256i*)&in[ 8*stride+0]), 1);
	t1 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 1*stride+0])), _mm256_load_si256((__m256i*)&in[ 9*stride+0]), 1);
	t2 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 2*stride+0])), _mm256_load_si256((__m256i*)&in[10*stride+0]), 1);
	t3 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 3*stride+0])), _mm256_load_si256((__m256i*)&in[11*stride+0]), 1);
	t4 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 4*stride+0])), _mm256_load_si256((__m256i*)&in[12*stride+0]), 1);
	t5 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 5*stride+0])), _mm256_load_si256((__m256i*)&in[13*stride+0]), 1);
	t6 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 6*stride+0])), _mm256_load_si256((__m256i*)&in[14*stride+0]), 1);
	t7 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 7*stride+0])), _mm256_load_si256((__m256i*)&in[15*stride+0]), 1);

	t8 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 0*stride+8])), _mm256_load_si256((__m256i*)&in[ 8*stride+8]), 1);
	t9 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 1*stride+8])), _mm256_load_si256((__m256i*)&in[ 9*stride+8]), 1);
	ta = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 2*stride+8])), _mm256_load_si256((__m256i*)&in[10*stride+8]), 1);
	tb = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 3*stride+8])), _mm256_load_si256((__m256i*)&in[11*stride+8]), 1);
	tc = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 4*stride+8])), _mm256_load_si256((__m256i*)&in[12*stride+8]), 1);
	td = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 5*stride+8])), _mm256_load_si256((__m256i*)&in[13*stride+8]), 1);
	te = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 6*stride+8])), _mm256_load_si256((__m256i*)&in[14*stride+8]), 1);
	tf = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 7*stride+8])), _mm256_load_si256((__m256i*)&in[15*stride+8]), 1);

	mask= 0xcc;
	r0 = _mm512_mask_permutexvar_epi64(t0, (__mmask8)mask, vidx1, t4);
	r1 = _mm512_mask_permutexvar_epi64(t1, (__mmask8)mask, vidx1, t5);
	r2 = _mm512_mask_permutexvar_epi64(t2, (__mmask8)mask, vidx1, t6);
	r3 = _mm512_mask_permutexvar_epi64(t3, (__mmask8)mask, vidx1, t7);
	r8 = _mm512_mask_permutexvar_epi64(t8, (__mmask8)mask, vidx1, tc);
	r9 = _mm512_mask_permutexvar_epi64(t9, (__mmask8)mask, vidx1, td);
	ra = _mm512_mask_permutexvar_epi64(ta, (__mmask8)mask, vidx1, te);
	rb = _mm512_mask_permutexvar_epi64(tb, (__mmask8)mask, vidx1, tf);

	mask= 0x33;
	r4 = _mm512_mask_permutexvar_epi64(t4, (__mmask8)mask, vidx1, t0);
	r5 = _mm512_mask_permutexvar_epi64(t5, (__mmask8)mask, vidx1, t1);
	r6 = _mm512_mask_permutexvar_epi64(t6, (__mmask8)mask, vidx1, t2);
	r7 = _mm512_mask_permutexvar_epi64(t7, (__mmask8)mask, vidx1, t3);
	rc = _mm512_mask_permutexvar_epi64(tc, (__mmask8)mask, vidx1, t8);
	rd = _mm512_mask_permutexvar_epi64(td, (__mmask8)mask, vidx1, t9);
	re = _mm512_mask_permutexvar_epi64(te, (__mmask8)mask, vidx1, ta);
	rf = _mm512_mask_permutexvar_epi64(tf, (__mmask8)mask, vidx1, tb);

	mask = 0xaa;
	t0 = _mm512_mask_permutexvar_epi64(r0, (__mmask8)mask, vidx2, r2);
	t1 = _mm512_mask_permutexvar_epi64(r1, (__mmask8)mask, vidx2, r3);
	t4 = _mm512_mask_permutexvar_epi64(r4, (__mmask8)mask, vidx2, r6);
	t5 = _mm512_mask_permutexvar_epi64(r5, (__mmask8)mask, vidx2, r7);
	t8 = _mm512_mask_permutexvar_epi64(r8, (__mmask8)mask, vidx2, ra);
	t9 = _mm512_mask_permutexvar_epi64(r9, (__mmask8)mask, vidx2, rb);
	tc = _mm512_mask_permutexvar_epi64(rc, (__mmask8)mask, vidx2, re);
	td = _mm512_mask_permutexvar_epi64(rd, (__mmask8)mask, vidx2, rf);

	mask = 0x55;
	t2 = _mm512_mask_permutexvar_epi64(r2, (__mmask8)mask, vidx2, r0);
	t3 = _mm512_mask_permutexvar_epi64(r3, (__mmask8)mask, vidx2, r1);
	t6 = _mm512_mask_permutexvar_epi64(r6, (__mmask8)mask, vidx2, r4);
	t7 = _mm512_mask_permutexvar_epi64(r7, (__mmask8)mask, vidx2, r5);
	ta = _mm512_mask_permutexvar_epi64(ra, (__mmask8)mask, vidx2, r8);
	tb = _mm512_mask_permutexvar_epi64(rb, (__mmask8)mask, vidx2, r9);
	te = _mm512_mask_permutexvar_epi64(re, (__mmask8)mask, vidx2, rc);
	tf = _mm512_mask_permutexvar_epi64(rf, (__mmask8)mask, vidx2, rd);

	mask = 0xaaaa;
	r0 = _mm512_mask_permutexvar_epi32(t0, (__mmask16)mask, vidx3, t1);
	r2 = _mm512_mask_permutexvar_epi32(t2, (__mmask16)mask, vidx3, t3);
	r4 = _mm512_mask_permutexvar_epi32(t4, (__mmask16)mask, vidx3, t5);
	r6 = _mm512_mask_permutexvar_epi32(t6, (__mmask16)mask, vidx3, t7);
	r8 = _mm512_mask_permutexvar_epi32(t8, (__mmask16)mask, vidx3, t9);
	ra = _mm512_mask_permutexvar_epi32(ta, (__mmask16)mask, vidx3, tb);
	rc = _mm512_mask_permutexvar_epi32(tc, (__mmask16)mask, vidx3, td);
	re = _mm512_mask_permutexvar_epi32(te, (__mmask16)mask, vidx3, tf);

	mask = 0x5555;
	r1 = _mm512_mask_permutexvar_epi32(t1, (__mmask16)mask, vidx3, t0);
	r3 = _mm512_mask_permutexvar_epi32(t3, (__mmask16)mask, vidx3, t2);
	r5 = _mm512_mask_permutexvar_epi32(t5, (__mmask16)mask, vidx3, t4);
	r7 = _mm512_mask_permutexvar_epi32(t7, (__mmask16)mask, vidx3, t6);
	r9 = _mm512_mask_permutexvar_epi32(t9, (__mmask16)mask, vidx3, t8);
	rb = _mm512_mask_permutexvar_epi32(tb, (__mmask16)mask, vidx3, ta);
	rd = _mm512_mask_permutexvar_epi32(td, (__mmask16)mask, vidx3, tc);
	rf = _mm512_mask_permutexvar_epi32(tf, (__mmask16)mask, vidx3, te);

	_mm512_store_epi32(&out[ 0*16], r0);
	_mm512_store_epi32(&out[ 1*16], r1);
	_mm512_store_epi32(&out[ 2*16], r2);
	_mm512_store_epi32(&out[ 3*16], r3);
	_mm512_store_epi32(&out[ 4*16], r4);
	_mm512_store_epi32(&out[ 5*16], r5);
	_mm512_store_epi32(&out[ 6*16], r6);
	_mm512_store_epi32(&out[ 7*16], r7);
	_mm512_store_epi32(&out[ 8*16], r8);
	_mm512_store_epi32(&out[ 9*16], r9);
	_mm512_store_epi32(&out[10*16], ra);
	_mm512_store_epi32(&out[11*16], rb);
	_mm512_store_epi32(&out[12*16], rc);
	_mm512_store_epi32(&out[13*16], rd);
	_mm512_store_epi32(&out[14*16], re);
	_mm512_store_epi32(&out[15*16], rf);
#endif
}

inline static void
LTW (const float *const __restrict__ in, float *const __restrict__ out, const int stride)
{
#if 0 == _IMPL_
	const int stride4 = stride * 4;
	{
		const float *const __restrict__ data0 = in;

		__m512 a0;
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data0), 0);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data0 + stride4 * 1), 1);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data0 + stride4 * 2), 2);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data0 + stride4 * 3), 3);
		__m512 a1;
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data0 + stride * 1), 0);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data0 + stride * 1 + stride4 * 1), 1);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data0 + stride * 1 + stride4 * 2), 2);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data0 + stride * 1 + stride4 * 3), 3);
		__m512 a2;
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data0 + stride * 2), 0);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data0 + stride * 2 + stride4 * 1), 1);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data0 + stride * 2 + stride4 * 2), 2);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data0 + stride * 2 + stride4 * 3), 3);
		__m512 a3;
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data0 + stride * 3), 0);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data0 + stride * 3 + stride4 * 1), 1);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data0 + stride * 3 + stride4 * 2), 2);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data0 + stride * 3 + stride4 * 3), 3);

		const __m512 b0 = _mm512_unpacklo_ps (a0, a2);
		const __m512 b1 = _mm512_unpackhi_ps (a0, a2);
		const __m512 b2 = _mm512_unpacklo_ps (a1, a3);
		const __m512 b3 = _mm512_unpackhi_ps (a1, a3);

		const __m512 c0 = _mm512_unpacklo_ps (b0, b2);
		const __m512 c1 = _mm512_unpackhi_ps (b0, b2);
		const __m512 c2 = _mm512_unpacklo_ps (b1, b3);
		const __m512 c3 = _mm512_unpackhi_ps (b1, b3);


		_mm512_storeu_ps (out + stride * 0, c0);
		_mm512_storeu_ps (out + stride * 1, c1);
		_mm512_storeu_ps (out + stride * 2, c2);
		_mm512_storeu_ps (out + stride * 3, c3);
	}
	{
		const float *const __restrict__ data1 = in + 4 * 1;

		__m512 a0;
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data1), 0);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data1 + stride4 * 1), 1);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data1 + stride4 * 2), 2);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data1 + stride4 * 3), 3);
		__m512 a1;
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data1 + stride * 1), 0);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data1 + stride * 1 + stride4 * 1), 1);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data1 + stride * 1 + stride4 * 2), 2);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data1 + stride * 1 + stride4 * 3), 3);
		__m512 a2;
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data1 + stride * 2), 0);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data1 + stride * 2 + stride4 * 1), 1);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data1 + stride * 2 + stride4 * 2), 2);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data1 + stride * 2 + stride4 * 3), 3);
		__m512 a3;
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data1 + stride * 3), 0);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data1 + stride * 3 + stride4 * 1), 1);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data1 + stride * 3 + stride4 * 2), 2);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data1 + stride * 3 + stride4 * 3), 3);

		const __m512 b0 = _mm512_unpacklo_ps (a0, a2);
		const __m512 b1 = _mm512_unpackhi_ps (a0, a2);
		const __m512 b2 = _mm512_unpacklo_ps (a1, a3);
		const __m512 b3 = _mm512_unpackhi_ps (a1, a3);

		const __m512 c0 = _mm512_unpacklo_ps (b0, b2);
		const __m512 c1 = _mm512_unpackhi_ps (b0, b2);
		const __m512 c2 = _mm512_unpacklo_ps (b1, b3);
		const __m512 c3 = _mm512_unpackhi_ps (b1, b3);


		_mm512_storeu_ps (out + stride * 4, c0);
		_mm512_storeu_ps (out + stride * 5, c1);
		_mm512_storeu_ps (out + stride * 6, c2);
		_mm512_storeu_ps (out + stride * 7, c3);
	}
	{
		const float *const __restrict__ data2 = in + 4 * 2;

		__m512 a0;
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data2), 0);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data2 + stride4 * 1), 1);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data2 + stride4 * 2), 2);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data2 + stride4 * 3), 3);
		__m512 a1;
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data2 + stride * 1), 0);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data2 + stride * 1 + stride4 * 1), 1);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data2 + stride * 1 + stride4 * 2), 2);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data2 + stride * 1 + stride4 * 3), 3);
		__m512 a2;
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data2 + stride * 2), 0);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data2 + stride * 2 + stride4 * 1), 1);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data2 + stride * 2 + stride4 * 2), 2);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data2 + stride * 2 + stride4 * 3), 3);
		__m512 a3;
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data2 + stride * 3), 0);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data2 + stride * 3 + stride4 * 1), 1);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data2 + stride * 3 + stride4 * 2), 2);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data2 + stride * 3 + stride4 * 3), 3);

		const __m512 b0 = _mm512_unpacklo_ps (a0, a2);
		const __m512 b1 = _mm512_unpackhi_ps (a0, a2);
		const __m512 b2 = _mm512_unpacklo_ps (a1, a3);
		const __m512 b3 = _mm512_unpackhi_ps (a1, a3);

		const __m512 c0 = _mm512_unpacklo_ps (b0, b2);
		const __m512 c1 = _mm512_unpackhi_ps (b0, b2);
		const __m512 c2 = _mm512_unpacklo_ps (b1, b3);
		const __m512 c3 = _mm512_unpackhi_ps (b1, b3);


		_mm512_storeu_ps (out + stride * 8, c0);
		_mm512_storeu_ps (out + stride * 9, c1);
		_mm512_storeu_ps (out + stride * 10, c2);
		_mm512_storeu_ps (out + stride * 11, c3);
	}
	{
		const float *const __restrict__ data3 = in + 4 * 3;

		__m512 a0;
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data3), 0);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data3 + stride4 * 1), 1);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data3 + stride4 * 2), 2);
		a0 = _mm512_insertf32x4 (a0, _mm_loadu_ps (data3 + stride4 * 3), 3);
		__m512 a1;
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data3 + stride * 1), 0);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data3 + stride * 1 + stride4 * 1), 1);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data3 + stride * 1 + stride4 * 2), 2);
		a1 = _mm512_insertf32x4 (a1, _mm_loadu_ps (data3 + stride * 1 + stride4 * 3), 3);
		__m512 a2;
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data3 + stride * 2), 0);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data3 + stride * 2 + stride4 * 1), 1);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data3 + stride * 2 + stride4 * 2), 2);
		a2 = _mm512_insertf32x4 (a2, _mm_loadu_ps (data3 + stride * 2 + stride4 * 3), 3);
		__m512 a3;
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data3 + stride * 3), 0);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data3 + stride * 3 + stride4 * 1), 1);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data3 + stride * 3 + stride4 * 2), 2);
		a3 = _mm512_insertf32x4 (a3, _mm_loadu_ps (data3 + stride * 3 + stride4 * 3), 3);

		const __m512 b0 = _mm512_unpacklo_ps (a0, a2);
		const __m512 b1 = _mm512_unpackhi_ps (a0, a2);
		const __m512 b2 = _mm512_unpacklo_ps (a1, a3);
		const __m512 b3 = _mm512_unpackhi_ps (a1, a3);

		const __m512 c0 = _mm512_unpacklo_ps (b0, b2);
		const __m512 c1 = _mm512_unpackhi_ps (b0, b2);
		const __m512 c2 = _mm512_unpacklo_ps (b1, b3);
		const __m512 c3 = _mm512_unpackhi_ps (b1, b3);


		_mm512_storeu_ps (out + stride * 12, c0);
		_mm512_storeu_ps (out + stride * 13, c1);
		_mm512_storeu_ps (out + stride * 14, c2);
		_mm512_storeu_ps (out + stride * 15, c3);
	}
#elif 1 == _IMPL_
	__m512i a0;
	a0 = _mm512_inserti64x4 (a0, _mm256_loadu_si256((__m256i *)(in + stride * 0)), 0);
	a0 = _mm512_inserti64x4 (a0, _mm256_loadu_si256((__m256i *)(in + stride * 8)), 1);

	__m512i a1;
	a1 = _mm512_inserti64x4 (a1, _mm256_loadu_si256((__m256i *)(in + stride * 1)), 0);
	a1 = _mm512_inserti64x4 (a1, _mm256_loadu_si256((__m256i *)(in + stride * 9)), 1);

	__m512i a2;
	a2 = _mm512_inserti64x4 (a2, _mm256_loadu_si256((__m256i *)(in + stride * 2)), 0);
	a2 = _mm512_inserti64x4 (a2, _mm256_loadu_si256((__m256i *)(in + stride * 10)), 1);

	__m512i a3;
	a3 = _mm512_inserti64x4 (a3, _mm256_loadu_si256((__m256i *)(in + stride * 3)), 0);
	a3 = _mm512_inserti64x4 (a3, _mm256_loadu_si256((__m256i *)(in + stride * 11)), 1);

	__m512i p0, p1, p2, p3;

	{
		__m512i r0 = _mm512_unpacklo_epi32(a0, a2);
		__m512i r1 = _mm512_unpacklo_epi32(a1, a3);

		p0 = _mm512_unpacklo_epi32(r0, r1);
		p1 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i r0 = _mm512_unpackhi_epi32(a0, a2);
		__m512i r1 = _mm512_unpackhi_epi32(a1, a3);

		p2 = _mm512_unpacklo_epi32(r0, r1);
		p3 = _mm512_unpackhi_epi32(r0, r1);
	}

	__m512i b0;
	b0 = _mm512_inserti64x4 (b0, _mm256_loadu_si256((__m256i *)(in + stride * 4)), 0);
	b0 = _mm512_inserti64x4 (b0, _mm256_loadu_si256((__m256i *)(in + stride * 12)), 1);

	__m512i b1;
	b1 = _mm512_inserti64x4 (b1, _mm256_loadu_si256((__m256i *)(in + stride * 5)), 0);
	b1 = _mm512_inserti64x4 (b1, _mm256_loadu_si256((__m256i *)(in + stride * 13)), 1);

	__m512i b2;
	b2 = _mm512_inserti64x4 (b2, _mm256_loadu_si256((__m256i *)(in + stride * 6)), 0);
	b2 = _mm512_inserti64x4 (b2, _mm256_loadu_si256((__m256i *)(in + stride * 14)), 1);

	__m512i b3;
	b3 = _mm512_inserti64x4 (b3, _mm256_loadu_si256((__m256i *)(in + stride * 7)), 0);
	b3 = _mm512_inserti64x4 (b3, _mm256_loadu_si256((__m256i *)(in + stride * 15)), 1);

	__m512i q0, q1, q2, q3;
	{
		__m512i r0 = _mm512_unpacklo_epi32(b0, b2);
		__m512i r1 = _mm512_unpacklo_epi32(b1, b3);

		q0 = _mm512_unpacklo_epi32(r0, r1);
		q1 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i r0 = _mm512_unpackhi_epi32(b0, b2);
		__m512i r1 = _mm512_unpackhi_epi32(b1, b3);

		q2 = _mm512_unpacklo_epi32(r0, r1);
		q3 = _mm512_unpackhi_epi32(r0, r1);
	}

	/* swap the 4x4 tiles */
	a0 = _mm512_inserti64x4 (a0, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 0)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p0, q0, 0x88);
		_mm512_storeu_si512(out, _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	a1 = _mm512_inserti64x4 (a1, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 1)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p1, q1, 0x88);
		_mm512_storeu_si512(out + stride * 1,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	a2 = _mm512_inserti64x4 (a2, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 2)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p2, q2, 0x88);
		_mm512_storeu_si512(out + stride * 2,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	a3 = _mm512_inserti64x4 (a3, _mm256_loadu_si256((__m256i *)(in + 8 +  stride * 3)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p3, q3, 0x88);
		_mm512_storeu_si512(out + stride * 3,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	b0 = _mm512_inserti64x4 (b0, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 4)), 0);
	{
		__m512i t0 = _mm512_shuffle_i32x4(p0, q0, 0xdd);
		_mm512_storeu_si512(out + stride * 4,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	b1 = _mm512_inserti64x4 (b1, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 5)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p1, q1, 0xdd);
		_mm512_storeu_si512(out + stride * 5,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	b2 = _mm512_inserti64x4 (b2, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 6)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p2, q2, 0xdd);
		_mm512_storeu_si512(out + stride * 6,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	b3 = _mm512_inserti64x4 (b3, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 7)), 0);

	{
		__m512i t0 = _mm512_shuffle_i32x4(p3, q3, 0xdd);
		_mm512_storeu_si512(out + stride * 7,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	a0 = _mm512_inserti64x4 (a0, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 8)), 1);
	a1 = _mm512_inserti64x4 (a1, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 9)), 1);
	a2 = _mm512_inserti64x4 (a2, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 10)), 1);
	a3 = _mm512_inserti64x4 (a3, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 11)), 1);

	b0 = _mm512_inserti64x4 (b0, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 12)), 1);
	b1 = _mm512_inserti64x4 (b1, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 13)), 1);
	b2 = _mm512_inserti64x4 (b2, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 14)), 1);
	b3 = _mm512_inserti64x4 (b3, _mm256_loadu_si256((__m256i *)(in + 8 + stride * 15)), 1);

	{
		__m512i r0 = _mm512_unpacklo_epi32(a0, a2);
		__m512i r1 = _mm512_unpacklo_epi32(a1, a3);

		p0 = _mm512_unpacklo_epi32(r0, r1);
		p1 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i r0 = _mm512_unpackhi_epi32(a0, a2);
		__m512i r1 = _mm512_unpackhi_epi32(a1, a3);

		p2 = _mm512_unpacklo_epi32(r0, r1);
		p3 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i r0 = _mm512_unpacklo_epi32(b0, b2);
		__m512i r1 = _mm512_unpacklo_epi32(b1, b3);

		q0 = _mm512_unpacklo_epi32(r0, r1);
		q1 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i r0 = _mm512_unpackhi_epi32(b0, b2);
		__m512i r1 = _mm512_unpackhi_epi32(b1, b3);

		q2 = _mm512_unpacklo_epi32(r0, r1);
		q3 = _mm512_unpackhi_epi32(r0, r1);
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p0, q0, 0x88);
		_mm512_storeu_si512(out + stride * 8, _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p1, q1, 0x88);
		_mm512_storeu_si512(out + stride * 9,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p2, q2, 0x88);
		_mm512_storeu_si512(out + stride * 10,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p3, q3, 0x88);
		_mm512_storeu_si512(out + stride * 11,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p0, q0, 0xdd);
		_mm512_storeu_si512(out + stride * 12,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p1, q1, 0xdd);
		_mm512_storeu_si512(out + stride * 13,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p2, q2, 0xdd);
		_mm512_storeu_si512(out + stride * 14,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}

	{
		__m512i t0 = _mm512_shuffle_i32x4(p3, q3, 0xdd);
		_mm512_storeu_si512(out + stride * 15,  _mm512_shuffle_i32x4(t0, t0,  0xd8));
	}
#elif 2 == _IMPL_
	__m512i t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, ta, tb, tc, td, te, tf;
	__m512i r0 = _mm512_loadu_si512(in + stride * 0);
	__m512i r1 = _mm512_loadu_si512(in + stride * 1);
	__m512i r2 = _mm512_loadu_si512(in + stride * 2);
	__m512i r3 = _mm512_loadu_si512(in + stride * 3);
	__m512i r4 = _mm512_loadu_si512(in + stride * 4);
	__m512i r5 = _mm512_loadu_si512(in + stride * 5);
	__m512i r6 = _mm512_loadu_si512(in + stride * 6);
	__m512i r7 = _mm512_loadu_si512(in + stride * 7);
	__m512i r8 = _mm512_loadu_si512(in + stride * 8);
	__m512i r9 = _mm512_loadu_si512(in + stride * 9);
	__m512i ra = _mm512_loadu_si512(in + stride * 10);
	__m512i rb = _mm512_loadu_si512(in + stride * 11);
	__m512i rc = _mm512_loadu_si512(in + stride * 12);
	__m512i rd = _mm512_loadu_si512(in + stride * 13);
	__m512i re = _mm512_loadu_si512(in + stride * 14);
	__m512i rf = _mm512_loadu_si512(in + stride * 15);

	t0 = _mm512_unpacklo_epi32(r0,r1); //   0  16   1  17   4  20   5  21   8  24   9  25  12  28  13  29
	t1 = _mm512_unpackhi_epi32(r0,r1); //   2  18   3  19   6  22   7  23  10  26  11  27  14  30  15  31
	t2 = _mm512_unpacklo_epi32(r2,r3); //  32  48  33  49 ...
	t3 = _mm512_unpackhi_epi32(r2,r3); //  34  50  35  51 ...
	t4 = _mm512_unpacklo_epi32(r4,r5); //  64  80  65  81 ...
	t5 = _mm512_unpackhi_epi32(r4,r5); //  66  82  67  83 ...
	t6 = _mm512_unpacklo_epi32(r6,r7); //  96 112  97 113 ...
	t7 = _mm512_unpackhi_epi32(r6,r7); //  98 114  99 115 ...
	t8 = _mm512_unpacklo_epi32(r8,r9); // 128 ...
	t9 = _mm512_unpackhi_epi32(r8,r9); // 130 ...
	ta = _mm512_unpacklo_epi32(ra,rb); // 160 ...
	tb = _mm512_unpackhi_epi32(ra,rb); // 162 ...
	tc = _mm512_unpacklo_epi32(rc,rd); // 196 ...
	td = _mm512_unpackhi_epi32(rc,rd); // 198 ...
	te = _mm512_unpacklo_epi32(re,rf); // 228 ...
	tf = _mm512_unpackhi_epi32(re,rf); // 230 ...

	r0 = _mm512_unpacklo_epi64(t0,t2); //   0  16  32  48 ...
	r1 = _mm512_unpackhi_epi64(t0,t2); //   1  17  33  49 ...
	r2 = _mm512_unpacklo_epi64(t1,t3); //   2  18  34  49 ...
	r3 = _mm512_unpackhi_epi64(t1,t3); //   3  19  35  51 ...
	r4 = _mm512_unpacklo_epi64(t4,t6); //  64  80  96 112 ...
	r5 = _mm512_unpackhi_epi64(t4,t6); //  65  81  97 114 ...
	r6 = _mm512_unpacklo_epi64(t5,t7); //  66  82  98 113 ...
	r7 = _mm512_unpackhi_epi64(t5,t7); //  67  83  99 115 ...
	r8 = _mm512_unpacklo_epi64(t8,ta); // 128 144 160 176 ...
	r9 = _mm512_unpackhi_epi64(t8,ta); // 129 145 161 178 ...
	ra = _mm512_unpacklo_epi64(t9,tb); // 130 146 162 177 ...
	rb = _mm512_unpackhi_epi64(t9,tb); // 131 147 163 179 ...
	rc = _mm512_unpacklo_epi64(tc,te); // 192 208 228 240 ...
	rd = _mm512_unpackhi_epi64(tc,te); // 193 209 229 241 ...
	re = _mm512_unpacklo_epi64(td,tf); // 194 210 230 242 ...
	rf = _mm512_unpackhi_epi64(td,tf); // 195 211 231 243 ...

	t0 = _mm512_shuffle_i32x4(r0, r4, 0x88); //   0  16  32  48   8  24  40  56  64  80  96  112 ...
	t1 = _mm512_shuffle_i32x4(r1, r5, 0x88); //   1  17  33  49 ...
	t2 = _mm512_shuffle_i32x4(r2, r6, 0x88); //   2  18  34  50 ...
	t3 = _mm512_shuffle_i32x4(r3, r7, 0x88); //   3  19  35  51 ...
	t4 = _mm512_shuffle_i32x4(r0, r4, 0xdd); //   4  20  36  52 ...
	t5 = _mm512_shuffle_i32x4(r1, r5, 0xdd); //   5  21  37  53 ...
	t6 = _mm512_shuffle_i32x4(r2, r6, 0xdd); //   6  22  38  54 ...
	t7 = _mm512_shuffle_i32x4(r3, r7, 0xdd); //   7  23  39  55 ...
	t8 = _mm512_shuffle_i32x4(r8, rc, 0x88); // 128 144 160 176 ...
	t9 = _mm512_shuffle_i32x4(r9, rd, 0x88); // 129 145 161 177 ...
	ta = _mm512_shuffle_i32x4(ra, re, 0x88); // 130 146 162 178 ...
	tb = _mm512_shuffle_i32x4(rb, rf, 0x88); // 131 147 163 179 ...
	tc = _mm512_shuffle_i32x4(r8, rc, 0xdd); // 132 148 164 180 ...
	td = _mm512_shuffle_i32x4(r9, rd, 0xdd); // 133 149 165 181 ...
	te = _mm512_shuffle_i32x4(ra, re, 0xdd); // 134 150 166 182 ...
	tf = _mm512_shuffle_i32x4(rb, rf, 0xdd); // 135 151 167 183 ...

	r0 = _mm512_shuffle_i32x4(t0, t8, 0x88); //   0  16  32  48  64  80  96 112 ... 240
	r1 = _mm512_shuffle_i32x4(t1, t9, 0x88); //   1  17  33  49  66  81  97 113 ... 241
	r2 = _mm512_shuffle_i32x4(t2, ta, 0x88); //   2  18  34  50  67  82  98 114 ... 242
	r3 = _mm512_shuffle_i32x4(t3, tb, 0x88); //   3  19  35  51  68  83  99 115 ... 243
	r4 = _mm512_shuffle_i32x4(t4, tc, 0x88); //   4 ...
	r5 = _mm512_shuffle_i32x4(t5, td, 0x88); //   5 ...
	r6 = _mm512_shuffle_i32x4(t6, te, 0x88); //   6 ...
	r7 = _mm512_shuffle_i32x4(t7, tf, 0x88); //   7 ...
	r8 = _mm512_shuffle_i32x4(t0, t8, 0xdd); //   8 ...
	r9 = _mm512_shuffle_i32x4(t1, t9, 0xdd); //   9 ...
	ra = _mm512_shuffle_i32x4(t2, ta, 0xdd); //  10 ...
	rb = _mm512_shuffle_i32x4(t3, tb, 0xdd); //  11 ...
	rc = _mm512_shuffle_i32x4(t4, tc, 0xdd); //  12 ...
	rd = _mm512_shuffle_i32x4(t5, td, 0xdd); //  13 ...
	re = _mm512_shuffle_i32x4(t6, te, 0xdd); //  14 ...
	rf = _mm512_shuffle_i32x4(t7, tf, 0xdd); //  15  31  47  63  79  96 111 127 ... 255

	_mm512_storeu_si512(out + stride * 0, r0);
	_mm512_storeu_si512(out + stride * 1, r1);
	_mm512_storeu_si512(out + stride * 2, r2);
	_mm512_storeu_si512(out + stride * 3, r3);
	_mm512_storeu_si512(out + stride * 4, r4);
	_mm512_storeu_si512(out + stride * 5, r5);
	_mm512_storeu_si512(out + stride * 6, r6);
	_mm512_storeu_si512(out + stride * 7, r7);
	_mm512_storeu_si512(out + stride * 8, r8);
	_mm512_storeu_si512(out + stride * 9, r9);
	_mm512_storeu_si512(out + stride * 10, ra);
	_mm512_storeu_si512(out + stride * 11, rb);
	_mm512_storeu_si512(out + stride * 12, rc);
	_mm512_storeu_si512(out + stride * 13, rd);
	_mm512_storeu_si512(out + stride * 14, re);
	_mm512_storeu_si512(out + stride * 15, rf);
#else
	__m512i t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, ta, tb, tc, td, te, tf;
	__m512i r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, ra, rb, rc, rd, re, rf;

	int mask;
	int64_t idx1[8] __attribute__((aligned(64))) = {2, 3, 0, 1, 6, 7, 4, 5};
	int64_t idx2[8] __attribute__((aligned(64))) = {1, 0, 3, 2, 5, 4, 7, 6};
	int32_t idx3[16] __attribute__((aligned(64))) = {1, 0, 3, 2, 5 ,4 ,7 ,6 ,9 ,8 , 11, 10, 13, 12 ,15, 14};
	__m512i vidx1 = _mm512_load_epi64(idx1);
	__m512i vidx2 = _mm512_load_epi64(idx2);
	__m512i vidx3 = _mm512_load_epi32(idx3);

	t0 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 0*stride+0])), _mm256_load_si256((__m256i*)&in[ 8*stride+0]), 1);
	t1 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 1*stride+0])), _mm256_load_si256((__m256i*)&in[ 9*stride+0]), 1);
	t2 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 2*stride+0])), _mm256_load_si256((__m256i*)&in[10*stride+0]), 1);
	t3 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 3*stride+0])), _mm256_load_si256((__m256i*)&in[11*stride+0]), 1);
	t4 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 4*stride+0])), _mm256_load_si256((__m256i*)&in[12*stride+0]), 1);
	t5 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 5*stride+0])), _mm256_load_si256((__m256i*)&in[13*stride+0]), 1);
	t6 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 6*stride+0])), _mm256_load_si256((__m256i*)&in[14*stride+0]), 1);
	t7 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 7*stride+0])), _mm256_load_si256((__m256i*)&in[15*stride+0]), 1);

	t8 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 0*stride+8])), _mm256_load_si256((__m256i*)&in[ 8*stride+8]), 1);
	t9 = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 1*stride+8])), _mm256_load_si256((__m256i*)&in[ 9*stride+8]), 1);
	ta = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 2*stride+8])), _mm256_load_si256((__m256i*)&in[10*stride+8]), 1);
	tb = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 3*stride+8])), _mm256_load_si256((__m256i*)&in[11*stride+8]), 1);
	tc = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 4*stride+8])), _mm256_load_si256((__m256i*)&in[12*stride+8]), 1);
	td = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 5*stride+8])), _mm256_load_si256((__m256i*)&in[13*stride+8]), 1);
	te = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 6*stride+8])), _mm256_load_si256((__m256i*)&in[14*stride+8]), 1);
	tf = _mm512_inserti64x4(_mm512_castsi256_si512(_mm256_load_si256((__m256i*)&in[ 7*stride+8])), _mm256_load_si256((__m256i*)&in[15*stride+8]), 1);

	mask= 0xcc;
	r0 = _mm512_mask_permutexvar_epi64(t0, (__mmask8)mask, vidx1, t4);
	r1 = _mm512_mask_permutexvar_epi64(t1, (__mmask8)mask, vidx1, t5);
	r2 = _mm512_mask_permutexvar_epi64(t2, (__mmask8)mask, vidx1, t6);
	r3 = _mm512_mask_permutexvar_epi64(t3, (__mmask8)mask, vidx1, t7);
	r8 = _mm512_mask_permutexvar_epi64(t8, (__mmask8)mask, vidx1, tc);
	r9 = _mm512_mask_permutexvar_epi64(t9, (__mmask8)mask, vidx1, td);
	ra = _mm512_mask_permutexvar_epi64(ta, (__mmask8)mask, vidx1, te);
	rb = _mm512_mask_permutexvar_epi64(tb, (__mmask8)mask, vidx1, tf);

	mask= 0x33;
	r4 = _mm512_mask_permutexvar_epi64(t4, (__mmask8)mask, vidx1, t0);
	r5 = _mm512_mask_permutexvar_epi64(t5, (__mmask8)mask, vidx1, t1);
	r6 = _mm512_mask_permutexvar_epi64(t6, (__mmask8)mask, vidx1, t2);
	r7 = _mm512_mask_permutexvar_epi64(t7, (__mmask8)mask, vidx1, t3);
	rc = _mm512_mask_permutexvar_epi64(tc, (__mmask8)mask, vidx1, t8);
	rd = _mm512_mask_permutexvar_epi64(td, (__mmask8)mask, vidx1, t9);
	re = _mm512_mask_permutexvar_epi64(te, (__mmask8)mask, vidx1, ta);
	rf = _mm512_mask_permutexvar_epi64(tf, (__mmask8)mask, vidx1, tb);

	mask = 0xaa;
	t0 = _mm512_mask_permutexvar_epi64(r0, (__mmask8)mask, vidx2, r2);
	t1 = _mm512_mask_permutexvar_epi64(r1, (__mmask8)mask, vidx2, r3);
	t4 = _mm512_mask_permutexvar_epi64(r4, (__mmask8)mask, vidx2, r6);
	t5 = _mm512_mask_permutexvar_epi64(r5, (__mmask8)mask, vidx2, r7);
	t8 = _mm512_mask_permutexvar_epi64(r8, (__mmask8)mask, vidx2, ra);
	t9 = _mm512_mask_permutexvar_epi64(r9, (__mmask8)mask, vidx2, rb);
	tc = _mm512_mask_permutexvar_epi64(rc, (__mmask8)mask, vidx2, re);
	td = _mm512_mask_permutexvar_epi64(rd, (__mmask8)mask, vidx2, rf);

	mask = 0x55;
	t2 = _mm512_mask_permutexvar_epi64(r2, (__mmask8)mask, vidx2, r0);
	t3 = _mm512_mask_permutexvar_epi64(r3, (__mmask8)mask, vidx2, r1);
	t6 = _mm512_mask_permutexvar_epi64(r6, (__mmask8)mask, vidx2, r4);
	t7 = _mm512_mask_permutexvar_epi64(r7, (__mmask8)mask, vidx2, r5);
	ta = _mm512_mask_permutexvar_epi64(ra, (__mmask8)mask, vidx2, r8);
	tb = _mm512_mask_permutexvar_epi64(rb, (__mmask8)mask, vidx2, r9);
	te = _mm512_mask_permutexvar_epi64(re, (__mmask8)mask, vidx2, rc);
	tf = _mm512_mask_permutexvar_epi64(rf, (__mmask8)mask, vidx2, rd);

	mask = 0xaaaa;
	r0 = _mm512_mask_permutexvar_epi32(t0, (__mmask16)mask, vidx3, t1);
	r2 = _mm512_mask_permutexvar_epi32(t2, (__mmask16)mask, vidx3, t3);
	r4 = _mm512_mask_permutexvar_epi32(t4, (__mmask16)mask, vidx3, t5);
	r6 = _mm512_mask_permutexvar_epi32(t6, (__mmask16)mask, vidx3, t7);
	r8 = _mm512_mask_permutexvar_epi32(t8, (__mmask16)mask, vidx3, t9);
	ra = _mm512_mask_permutexvar_epi32(ta, (__mmask16)mask, vidx3, tb);
	rc = _mm512_mask_permutexvar_epi32(tc, (__mmask16)mask, vidx3, td);
	re = _mm512_mask_permutexvar_epi32(te, (__mmask16)mask, vidx3, tf);

	mask = 0x5555;
	r1 = _mm512_mask_permutexvar_epi32(t1, (__mmask16)mask, vidx3, t0);
	r3 = _mm512_mask_permutexvar_epi32(t3, (__mmask16)mask, vidx3, t2);
	r5 = _mm512_mask_permutexvar_epi32(t5, (__mmask16)mask, vidx3, t4);
	r7 = _mm512_mask_permutexvar_epi32(t7, (__mmask16)mask, vidx3, t6);
	r9 = _mm512_mask_permutexvar_epi32(t9, (__mmask16)mask, vidx3, t8);
	rb = _mm512_mask_permutexvar_epi32(tb, (__mmask16)mask, vidx3, ta);
	rd = _mm512_mask_permutexvar_epi32(td, (__mmask16)mask, vidx3, tc);
	rf = _mm512_mask_permutexvar_epi32(tf, (__mmask16)mask, vidx3, te);

	_mm512_store_epi32(&out[ 0*stride], r0);
	_mm512_store_epi32(&out[ 1*stride], r1);
	_mm512_store_epi32(&out[ 2*stride], r2);
	_mm512_store_epi32(&out[ 3*stride], r3);
	_mm512_store_epi32(&out[ 4*stride], r4);
	_mm512_store_epi32(&out[ 5*stride], r5);
	_mm512_store_epi32(&out[ 6*stride], r6);
	_mm512_store_epi32(&out[ 7*stride], r7);
	_mm512_store_epi32(&out[ 8*stride], r8);
	_mm512_store_epi32(&out[ 9*stride], r9);
	_mm512_store_epi32(&out[10*stride], ra);
	_mm512_store_epi32(&out[11*stride], rb);
	_mm512_store_epi32(&out[12*stride], rc);
	_mm512_store_epi32(&out[13*stride], rd);
	_mm512_store_epi32(&out[14*stride], re);
	_mm512_store_epi32(&out[15*stride], rf);
#endif
}

inline static void
WRITE (const float *const __restrict__ in, float *const __restrict__ out, const int stride)
{
	_mm512_storeu_ps (out + stride * 0, _mm512_load_ps (in + TILE * 0));
	_mm512_storeu_ps (out + stride * 1, _mm512_load_ps (in + TILE * 1));
	_mm512_storeu_ps (out + stride * 2, _mm512_load_ps (in + TILE * 2));
	_mm512_storeu_ps (out + stride * 3, _mm512_load_ps (in + TILE * 3));
	_mm512_storeu_ps (out + stride * 4, _mm512_load_ps (in + TILE * 4));
	_mm512_storeu_ps (out + stride * 5, _mm512_load_ps (in + TILE * 5));
	_mm512_storeu_ps (out + stride * 6, _mm512_load_ps (in + TILE * 6));
	_mm512_storeu_ps (out + stride * 7, _mm512_load_ps (in + TILE * 7));
	_mm512_storeu_ps (out + stride * 8, _mm512_load_ps (in + TILE * 8));
	_mm512_storeu_ps (out + stride * 9, _mm512_load_ps (in + TILE * 9));
	_mm512_storeu_ps (out + stride * 10, _mm512_load_ps (in + TILE * 10));
	_mm512_storeu_ps (out + stride * 11, _mm512_load_ps (in + TILE * 11));
	_mm512_storeu_ps (out + stride * 12, _mm512_load_ps (in + TILE * 12));
	_mm512_storeu_ps (out + stride * 13, _mm512_load_ps (in + TILE * 13));
	_mm512_storeu_ps (out + stride * 14, _mm512_load_ps (in + TILE * 14));
	_mm512_storeu_ps (out + stride * 15, _mm512_load_ps (in + TILE * 15));
}
