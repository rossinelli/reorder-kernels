#pragma once

#include <stddef.h>
#ifndef _POSIX_C_SOURCE
#error: "error: please define _POSIX_C_SOURCE to be 200112L or later"
#elif _POSIX_C_SOURCE < 200112L
#error: "error: please define _POSIX_C_SOURCE to be 200112L or later"
#endif
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <unistd.h>
#ifdef __APPLE__
#define _DARWIN_C_SOURCE
#endif
#include <fcntl.h>

#ifndef _WIN32
#define fseeko64 fseek
#define ftello64 ftell
#endif

#define TOSTR_(a) #a
#define MKSTR(a) TOSTR_(a)

#define POSIX_CHECK(stmt)                            \
	do                                               \
{                                                \
	if (!(stmt))                                 \
	{                                            \
		perror(#stmt  " in "                     \
				__FILE__ ":" MKSTR(__LINE__) );   \
		\
		abort();                                 \
		exit(EXIT_FAILURE);                      \
	}                                            \
}                                                \
while(0)

#define CHECK(stmt, ...)                        \
	do                                            \
{                                            \
	if (!(stmt))                            \
	{                                        \
		fprintf(stderr,                        \
				__VA_ARGS__);                \
		\
		exit(EXIT_FAILURE);                    \
	}                                        \
}                                            \
while(0)

#include <sys/time.h>

static inline double rdtss(void)
{
	struct timeval tv;
	POSIX_CHECK(0 == gettimeofday(&tv, NULL));

	return tv.tv_sec + 1e-6 * tv.tv_usec;
}

/* count entries from FILE */
static inline
size_t ceff_posix (
		FILE * f,
		/* single element size */
		const size_t esz )
{
	size_t old = ftello64(f);

	POSIX_CHECK(0 == fseeko64(f, 0, SEEK_END));

	size_t fsz = ftello64(f);

	POSIX_CHECK(0 == fseeko64(f, old, SEEK_SET));

	if (fsz % esz)
	{
		enum { CAP = 1 << 12 };
		char path[CAP] = { 0 };

		int fd;
		POSIX_CHECK(-1 != (fd = fileno(f)));

#ifdef __APPLE__
		POSIX_CHECK(-1 != fcntl(fd, F_GETPATH, path));
#else
		char tmp[CAP];
		POSIX_CHECK(snprintf(tmp, sizeof(tmp), "/proc/self/fd/%d", fd));
		POSIX_CHECK(-1 != readlink(path, tmp, sizeof(tmp)));
#endif
		fprintf(stderr,
				"error: size (%zd) of <%s> not multiple of element size (%zd)\n",
				fsz, path, esz);
	}

	return fsz / esz;
}

/* count entries from pathname */
static inline
size_t cefp_posix (
		const char * const path,
		/* single element size */
		const size_t esz )
{
	FILE * f;
	POSIX_CHECK(f = fopen(path, "rb"));

	const size_t retval = ceff_posix (f, esz);

	POSIX_CHECK(0 == fclose(f));

	return retval;
}

#include <stdarg.h>

/* count entries from multiple pathnames */
static ptrdiff_t cefmpv_posix (
		/* single entry size in bytes */
		const ptrdiff_t esz,
		/* pathname count */
		const int pc,
		/* pathnames */
		const char * const * pn)
{
	if (!pc)
		return 0;

	const char * refpname = pn[0];

	ptrdiff_t retval = 0;

	for (int p = 0; p < pc; ++p)
	{
		const ptrdiff_t c = cefp_posix(pn[p], esz);

		if (p)
			CHECK(c == retval,
					"error: file size in <%s> (%zd) different than <%s> (%zd)\n",
					pn[p], c * esz, refpname, retval);

		retval = c;
	}

	return retval;
}

/* count entries from multiple pathnames */
static ptrdiff_t cefmp_posix (
		/* single entry size in bytes */
		const ptrdiff_t esz,
		/* pathname count */
		const int pc,
		/* entries are (const char *) */
		... )
{
	const char ** pn = NULL;
	POSIX_CHECK(pn = (const char **)malloc(sizeof(*pn) * pc));

	va_list ptr;
	va_start(ptr, pc);

	for (int p = 0; p < pc; ++p)
		pn[p] = va_arg(ptr, const char *);

	va_end(ptr);

	const ptrdiff_t retval = cefmpv_posix(esz, pc, pn);

	return retval;
}

static inline size_t count_slices_posix (FILE * f, size_t slcsz) { return ceff_posix(f, slcsz); }

static inline size_t count_slices_pathname_posix (const char * path, size_t slcsz) { return cefp_posix(path, slcsz); }

static ptrdiff_t rstream_posix
(
 const char * const pathname,
 const ptrdiff_t esz,
 void ** data
 )
{
	FILE * f;
	POSIX_CHECK(f = fopen(pathname, "rb"));

	const ptrdiff_t fsz = ceff_posix(f, 1);

	POSIX_CHECK(*data = malloc(fsz));

	if (fsz)
		POSIX_CHECK(1 == fread(*data, fsz, 1, f));

	POSIX_CHECK(0 == fclose(f));

	return fsz / esz;
}

static void wstream_posix
(
 const char * const path,
 const ptrdiff_t sz,
 const void * data
 )
{
	FILE * f;
	POSIX_CHECK(f = fopen(path, "wb"));

	if (sz)
		POSIX_CHECK(1 == fwrite(data, sz, 1, f));

	POSIX_CHECK(0 == fclose(f));
}

static void wstream_posix_ext
(
 const char * const path,
 const ptrdiff_t sz,
 const void * data,
 const int append
 )
{
	FILE * f;
	POSIX_CHECK(f = fopen(path, append ? "ab" : "wb"));

	if (sz)
		POSIX_CHECK(1 == fwrite(data, sz, 1, f));

	POSIX_CHECK(0 == fclose(f));
}
