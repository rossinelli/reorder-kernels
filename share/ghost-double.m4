divert(-1)
define(`forloop', `pushdef(`$1', `$2')_forloop($@)popdef(`$1')')
define(`_forloop',
       `$4`'ifelse($1, `$3', `', `define(`$1', incr($1))$0($@)')')

define(`cat', `$1'`$2')
define(`entry', `eval($1 + 3 * ($2) + 9 * ($3))')
define(`opposite', `eval(26 - $1)')
divert(0)

enum { TILE = 8, TILE2 = TILE * TILE };

#include <immintrin.h>

inline static void LOAD_TRANSPOSED (
       const double * const __restrict__ in,
       double * const __restrict__ out,
       const int stride )
{
    const int stride2 = stride * 2;
    forloop(base, 0, 3, `dnl
    {
	const double * const __restrict__ cat(data, base) = in ifelse(base,0,,`+ 2 * base') ;

	forloop(j, 0, 1, `dnl
	__m512d cat(a, j);
	{__m256d tmp0, tmp1;
	forloop(i, 0, 3, `dnl
	       cat(tmp, eval(i / 2)) = _mm256_insertf128_pd(cat(tmp, eval(i / 2)), dnl
	       		_mm_loadu_pd(cat(data, base) ifelse(j,0,,`+ stride * j') ifelse(i,0,,`+ stride2 * i')), eval(i % 2));
	')
	cat(a, j) = _mm512_insertf64x4 (_mm512_castpd256_pd512(tmp0), tmp1, 1);
	}
	')
	const __m512d b0 = _mm512_unpacklo_pd(a0, a1);
	const __m512d b1 = _mm512_unpackhi_pd(a0, a1);

	forloop(j, 0, 1, `
		   _mm512_store_pd(out + TILE * eval(2 * base + j), cat(b, j));')
    }')
}

inline static void LTW (
       const double * const __restrict__ in,
       double * const __restrict__ out,
       const int stride )
{
	const int stride2 = stride * 2;
    forloop(base, 0, 3, `dnl
    {
	const double * const __restrict__ cat(data, base) = in ifelse(base,0,,`+ 2 * base') ;

	__m256d tmp0, tmp1;
	forloop(j, 0, 1, `dnl
	forloop(i, 0, 3, `dnl
	       cat(tmp, eval(i / 2)) = _mm256_insertf128_pd(cat(tmp, eval(i / 2)), dnl
	       		_mm_loadu_pd(cat(data, base) ifelse(j,0,,`+ stride * j') ifelse(i,0,,`+ stride2 * i')), eval(i % 2));
	')
	__m512d cat(a, j) = _mm512_insertf64x4 (_mm512_castpd256_pd512(tmp0), tmp1, 1);
	')

	const __m512d b0 = _mm512_unpacklo_pd(a0, a1);
	const __m512d b1 = _mm512_unpackhi_pd(a0, a1);

	forloop(j, 0, 1, `
		   _mm512_storeu_pd(out + stride * eval(2 * base + j), cat(b, j)) ;')
    }')
}

inline static void WRITE (
       const double * const __restrict__ in,
       double * const __restrict__ out,
       const int stride )
{
    forloop(i, 0, 7, `
    _mm512_storeu_pd(out + stride * i, _mm512_load_pd(in + TILE * i));')
}
