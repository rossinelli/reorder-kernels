enum
{ TILE = 8, TILE2 = TILE * TILE };

#include <immintrin.h>

inline static void
LOAD_TRANSPOSED (const double *const __restrict__ in, double *const __restrict__ out, const int stride)
{
	const int stride2 = stride * 2;
	{
		const double *const __restrict__ data0 = in;

		__m512d a0;
		{
			__m256d tmp0, tmp1;
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data0), 0);
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data0 + stride2 * 1), 1);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data0 + stride2 * 2), 0);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data0 + stride2 * 3), 1);

			a0 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		}
		__m512d a1;
		{
			__m256d tmp0, tmp1;
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data0 + stride * 1), 0);
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data0 + stride * 1 + stride2 * 1), 1);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data0 + stride * 1 + stride2 * 2), 0);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data0 + stride * 1 + stride2 * 3), 1);

			a1 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		}

		const __m512d b0 = _mm512_unpacklo_pd (a0, a1);
		const __m512d b1 = _mm512_unpackhi_pd (a0, a1);


		_mm512_store_pd (out + TILE * 0, b0);
		_mm512_store_pd (out + TILE * 1, b1);
	}
	{
		const double *const __restrict__ data1 = in + 2 * 1;

		__m512d a0;
		{
			__m256d tmp0, tmp1;
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data1), 0);
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data1 + stride2 * 1), 1);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data1 + stride2 * 2), 0);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data1 + stride2 * 3), 1);

			a0 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		}
		__m512d a1;
		{
			__m256d tmp0, tmp1;
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data1 + stride * 1), 0);
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data1 + stride * 1 + stride2 * 1), 1);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data1 + stride * 1 + stride2 * 2), 0);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data1 + stride * 1 + stride2 * 3), 1);

			a1 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		}

		const __m512d b0 = _mm512_unpacklo_pd (a0, a1);
		const __m512d b1 = _mm512_unpackhi_pd (a0, a1);


		_mm512_store_pd (out + TILE * 2, b0);
		_mm512_store_pd (out + TILE * 3, b1);
	}
	{
		const double *const __restrict__ data2 = in + 2 * 2;

		__m512d a0;
		{
			__m256d tmp0, tmp1;
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data2), 0);
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data2 + stride2 * 1), 1);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data2 + stride2 * 2), 0);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data2 + stride2 * 3), 1);

			a0 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		}
		__m512d a1;
		{
			__m256d tmp0, tmp1;
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data2 + stride * 1), 0);
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data2 + stride * 1 + stride2 * 1), 1);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data2 + stride * 1 + stride2 * 2), 0);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data2 + stride * 1 + stride2 * 3), 1);

			a1 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		}

		const __m512d b0 = _mm512_unpacklo_pd (a0, a1);
		const __m512d b1 = _mm512_unpackhi_pd (a0, a1);


		_mm512_store_pd (out + TILE * 4, b0);
		_mm512_store_pd (out + TILE * 5, b1);
	}
	{
		const double *const __restrict__ data3 = in + 2 * 3;

		__m512d a0;
		{
			__m256d tmp0, tmp1;
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data3), 0);
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data3 + stride2 * 1), 1);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data3 + stride2 * 2), 0);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data3 + stride2 * 3), 1);

			a0 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		}
		__m512d a1;
		{
			__m256d tmp0, tmp1;
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data3 + stride * 1), 0);
			tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data3 + stride * 1 + stride2 * 1), 1);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data3 + stride * 1 + stride2 * 2), 0);
			tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data3 + stride * 1 + stride2 * 3), 1);

			a1 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		}

		const __m512d b0 = _mm512_unpacklo_pd (a0, a1);
		const __m512d b1 = _mm512_unpackhi_pd (a0, a1);


		_mm512_store_pd (out + TILE * 6, b0);
		_mm512_store_pd (out + TILE * 7, b1);
	}
}

inline static void
LTW (const double *const __restrict__ in, double *const __restrict__ out, const int stride)
{
	const int stride2 = stride * 2;
	{
		const double *const __restrict__ data0 = in;

		__m256d tmp0, tmp1;
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data0), 0);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data0 + stride2 * 1), 1);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data0 + stride2 * 2), 0);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data0 + stride2 * 3), 1);

		__m512d a0 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data0 + stride * 1), 0);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data0 + stride * 1 + stride2 * 1), 1);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data0 + stride * 1 + stride2 * 2), 0);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data0 + stride * 1 + stride2 * 3), 1);

		__m512d a1 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);


		const __m512d b0 = _mm512_unpacklo_pd (a0, a1);
		const __m512d b1 = _mm512_unpackhi_pd (a0, a1);


		_mm512_storeu_pd (out + stride * 0, b0);
		_mm512_storeu_pd (out + stride * 1, b1);
	}
	{
		const double *const __restrict__ data1 = in + 2 * 1;

		__m256d tmp0, tmp1;
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data1), 0);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data1 + stride2 * 1), 1);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data1 + stride2 * 2), 0);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data1 + stride2 * 3), 1);

		__m512d a0 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data1 + stride * 1), 0);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data1 + stride * 1 + stride2 * 1), 1);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data1 + stride * 1 + stride2 * 2), 0);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data1 + stride * 1 + stride2 * 3), 1);

		__m512d a1 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);


		const __m512d b0 = _mm512_unpacklo_pd (a0, a1);
		const __m512d b1 = _mm512_unpackhi_pd (a0, a1);


		_mm512_storeu_pd (out + stride * 2, b0);
		_mm512_storeu_pd (out + stride * 3, b1);
	}
	{
		const double *const __restrict__ data2 = in + 2 * 2;

		__m256d tmp0, tmp1;
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data2), 0);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data2 + stride2 * 1), 1);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data2 + stride2 * 2), 0);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data2 + stride2 * 3), 1);

		__m512d a0 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data2 + stride * 1), 0);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data2 + stride * 1 + stride2 * 1), 1);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data2 + stride * 1 + stride2 * 2), 0);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data2 + stride * 1 + stride2 * 3), 1);

		__m512d a1 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);


		const __m512d b0 = _mm512_unpacklo_pd (a0, a1);
		const __m512d b1 = _mm512_unpackhi_pd (a0, a1);


		_mm512_storeu_pd (out + stride * 4, b0);
		_mm512_storeu_pd (out + stride * 5, b1);
	}
	{
		const double *const __restrict__ data3 = in + 2 * 3;

		__m256d tmp0, tmp1;
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data3), 0);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data3 + stride2 * 1), 1);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data3 + stride2 * 2), 0);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data3 + stride2 * 3), 1);

		__m512d a0 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data3 + stride * 1), 0);
		tmp0 = _mm256_insertf128_pd (tmp0, _mm_loadu_pd (data3 + stride * 1 + stride2 * 1), 1);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data3 + stride * 1 + stride2 * 2), 0);
		tmp1 = _mm256_insertf128_pd (tmp1, _mm_loadu_pd (data3 + stride * 1 + stride2 * 3), 1);

		__m512d a1 = _mm512_insertf64x4 (_mm512_castpd256_pd512 (tmp0), tmp1, 1);


		const __m512d b0 = _mm512_unpacklo_pd (a0, a1);
		const __m512d b1 = _mm512_unpackhi_pd (a0, a1);


		_mm512_storeu_pd (out + stride * 6, b0);
		_mm512_storeu_pd (out + stride * 7, b1);
	}
}

inline static void
WRITE (const double *const __restrict__ in, double *const __restrict__ out, const int stride)
{

	_mm512_storeu_pd (out + stride * 0, _mm512_load_pd (in + TILE * 0));
	_mm512_storeu_pd (out + stride * 1, _mm512_load_pd (in + TILE * 1));
	_mm512_storeu_pd (out + stride * 2, _mm512_load_pd (in + TILE * 2));
	_mm512_storeu_pd (out + stride * 3, _mm512_load_pd (in + TILE * 3));
	_mm512_storeu_pd (out + stride * 4, _mm512_load_pd (in + TILE * 4));
	_mm512_storeu_pd (out + stride * 5, _mm512_load_pd (in + TILE * 5));
	_mm512_storeu_pd (out + stride * 6, _mm512_load_pd (in + TILE * 6));
	_mm512_storeu_pd (out + stride * 7, _mm512_load_pd (in + TILE * 7));
}
