divert(-1)
define(`forloop', `pushdef(`$1', `$2')_forloop($@)popdef(`$1')')
define(`_forloop',
       `$4`'ifelse($1, `$3', `', `define(`$1', incr($1))$0($@)')')

define(`cat', `$1'`$2')
define(`entry', `eval($1 + 3 * ($2) + 9 * ($3))')
define(`opposite', `eval(26 - $1)')
divert(0)

enum { TILE = 16, TILE2 = TILE * TILE };

#include <immintrin.h>

inline static void LOAD_TRANSPOSED (
       const float * const __restrict__ in,
       float * const __restrict__ out,
       const int stride )
{
	const int stride4 = stride * 4;
    forloop(base, 0, 3, `dnl
    {
	const float * const __restrict__ cat(data, base) = in ifelse(base,0,,`+ 4 * base') ;

	forloop(j, 0, 3, `dnl
	__m512 cat(a, j);
	forloop(i, 0, 3, `
	cat(a, j) = _mm512_insertf32x4 (cat(a, j),dnl
	       _mm_loadu_ps(cat(data, base) ifelse(j,0,,`+ stride * j') ifelse(i,0,,`+ stride4 * i')), i);')')

	const __m512 b0 = _mm512_unpacklo_ps(a0, a2);
	const __m512 b1 = _mm512_unpackhi_ps(a0, a2);
	const __m512 b2 = _mm512_unpacklo_ps(a1, a3);
	const __m512 b3 = _mm512_unpackhi_ps(a1, a3);

	const __m512 c0 = _mm512_unpacklo_ps(b0, b2);
	const __m512 c1 = _mm512_unpackhi_ps(b0, b2);
	const __m512 c2 = _mm512_unpacklo_ps(b1, b3);
	const __m512 c3 = _mm512_unpackhi_ps(b1, b3);

	forloop(j, 0, 3, `
		   _mm512_store_ps(out + TILE * eval(4 * base + j), cat(c, j));')
    }')
}

inline static void LTW (
       const float * const __restrict__ in,
       float * const __restrict__ out,
       const int stride )
{
	const int stride4 = stride * 4;
    forloop(base, 0, 3, `dnl
    {
	const float * const __restrict__ cat(data, base) = in ifelse(base,0,,`+ 4 * base') ;

	forloop(j, 0, 3, `dnl
	__m512 cat(a, j);
	forloop(i, 0, 3, `dnl
	cat(a, j) = _mm512_insertf32x4 (cat(a, j),dnl
	       _mm_loadu_ps(cat(data, base) ifelse(j,0,,`+ stride * j') ifelse(i,0,,`+ stride4 * i')), i);')')

	const __m512 b0 = _mm512_unpacklo_ps(a0, a2);
	const __m512 b1 = _mm512_unpackhi_ps(a0, a2);
	const __m512 b2 = _mm512_unpacklo_ps(a1, a3);
	const __m512 b3 = _mm512_unpackhi_ps(a1, a3);

	const __m512 c0 = _mm512_unpacklo_ps(b0, b2);
	const __m512 c1 = _mm512_unpackhi_ps(b0, b2);
	const __m512 c2 = _mm512_unpacklo_ps(b1, b3);
	const __m512 c3 = _mm512_unpackhi_ps(b1, b3);

	forloop(j, 0, 3, `
		   _mm512_storeu_ps(out + stride * eval(4 * base + j), cat(c, j)) ;')
    }')
}

inline static void WRITE (
       const float * const __restrict__ in,
       float * const __restrict__ out,
       const int stride )
{
    forloop(i, 0, 15, `
    _mm512_storeu_ps(out + stride * i, _mm512_load_ps(in + TILE * i));')
}
